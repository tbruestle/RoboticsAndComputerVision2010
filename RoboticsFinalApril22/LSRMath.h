#pragma once
using namespace std;

class LSRMath{
protected:

	//Subtracts a multiple of a row from the other. Used in inversion.
	int SubRowMult(vector<vector<double>>& Ain, vector<vector<double>>& AInv, int Arow,int subrow,int col);

	//Divides a row by the value in an element of that row. Used in inversion
	int DivRow( vector<vector<double>>& Ain, vector<vector<double>>& AInv, int Arow, int divcol);

public:
	LSRMath();
	~LSRMath();

	//Inverts a matrix
	int InvertMatrix( vector<vector<double>> Ain, vector<vector<double>>& AInv);
	
	//Transposes a matrix
	int TransposeMatrix( vector<vector<double>> Aarr, vector<vector<double>>& Atarr);
	
	//Multiplies 2 matrices
	int MultiplyMatrix( vector<vector<double>> Aarr, vector<vector<double>> Barr, vector<vector<double>>& Carr);

	//Finds the cross product of a matrix
	int CrossProduct(vector<vector<double>> A, vector<vector<double>> B, vector<vector<double>>& AcB);

	//Normalizes a matrix. Returns the normalization in the old location
	int Normalize(vector<vector<double>>& A);
	
	//Takes the dot product of two n by 1 matrixes (vectors).
	int DotProduct(vector<vector<double>> A, vector<vector<double>> B, double& AdB);

	//Solves Least Squares Regression using q=inv(A'*A)*A'*B.
	int SolveLSR(vector<vector<double>> Aarr, vector<vector<double>> Barr, vector<vector<double>>& solveVar, char* errorFile = "");

	//Estimates second order polynomials for X(t),Y(t),Z(t) using SolveLSR.
	int SolveParabolicXYZt(vector<vector<double>> XYZarr, vector<double> Tarr, vector<vector<double>>& solveVar);

	//Solves Stereo Reconstruction using a given set of point pairs and the 2 M matrices
	//Uses the 6 line method I developed
	int solveStereoReconstruction( vector<vector<double>> M1, vector<vector<double>> M2, vector<double>& pPairs, vector<vector<double>>& coords );

	//Solves for the image coordinates given world coordinates and an M matrix
	int findImgCoordsFromWorld(vector<vector<double>> coords, vector<vector<double>> M, vector<vector<double>>& imCoords);

	//Finds a point at a given time T on the second order polynomials X(t),Y(t),Z(t) found with SolveParabolicXYZt
	int findParabolicPoint(vector<vector<double>> SolveVar, double t, vector<vector<double>>& coords);

	//Transforms a point with a given M Matrix
	int MMatrixTransform(vector<vector<double>> coords, vector<vector<double>> M, vector<vector<double>>& Mcoords);

	//I got approximate X & Y axes; I want the M matrix. This finds me one.
	int findMMatrixFromLaserAxes(vector<vector<double>> vLaserX, vector<vector<double>> cLaserX, vector<vector<double>> vLaserY, vector<vector<double>> cLaserY, vector<vector<double>>& M);

	//This tells me the lengths of a set of vectors.
	int FindLengths(vector<vector<double>> A, vector<double>& VectorLengths );

	//Returns the points (0,0,0), (1,0,0), (0,1,0), and (0,0,1)
	int findAxesPoints(vector<vector<vector<vector<double>>>>& XYZcoords, double length);

	//Returns the image transformation of the points (0,0,0), (1,0,0), (0,1,0), and (0,0,1)
	int findImageAxesPoints(vector<vector<vector<vector<double>>>> XYZcoords, vector<vector<double>> M, vector<vector<vector<vector<double>>>>& ImCoords);

	//Returns the Rotation Angles that make up the M Matrix
	int FindRotationAngles(  vector<vector<double>> MExt1, vector<double>& RotXYZ1 );

	//Outputs Error into a text file
	int LSRMath::CalculateLSRError( vector<vector<double>> Aarr, vector<vector<double>> Barr, vector<vector<double>> solveVar, char* errorFile );

};
