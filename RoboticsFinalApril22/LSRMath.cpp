#include <vector>
#include <iterator>
#include "LSRMath.h"
#include <math.h>
#include <stdio.h>
using namespace std;

#define M_PI       3.14159265358979323846
	

int LSRMath::solveStereoReconstruction( vector<vector<double>> M1, vector<vector<double>> M2, vector<double>& pPairs, vector<vector<double>>& coords )
{
	vector<vector<double>> Aarr( 6, vector<double>( 5 ) );
	vector<vector<double>> Barr( 6, vector<double>( 1 ) );

	for (int i = 0; i<3; i++)
	{
		Aarr[i][0]=M1[i][0];
		Aarr[i][1]=M1[i][1];
		Aarr[i][2]=M1[i][2];
		Barr[i][0]=-1*M1[i][3];
		Aarr[i][4]=0;
	}
		Aarr[0][3]=-1*pPairs[0];
		Aarr[1][3]=-1*pPairs[1];
		Aarr[2][3]=-1;

	for (int i = 0; i<3; i++)
	{
		Aarr[i+3][0]=M2[i][0];
		Aarr[i+3][1]=M2[i][1];
		Aarr[i+3][2]=M2[i][2];
		Barr[i+3][0]=-1*M2[i][3];
		Aarr[i+3][3]=0;
	}
		Aarr[3][4]=-1*pPairs[2];
		Aarr[4][4]=-1*pPairs[3];
		Aarr[5][4]=-1;

	if (!SolveLSR(Aarr,Barr,coords,"")==0) { return -1; }
	return 0;
}

int LSRMath::SubRowMult(vector<vector<double>>& Ain, vector<vector<double>>& AInv, int Arow,int subrow,int col)
{
	int Asize = (int) Ain.size();
	double mult = Ain[Arow][col]/Ain[subrow][col];
	for (int i = 0; i<Asize; i++)
	{
		Ain[Arow][i]=Ain[Arow][i]-Ain[subrow][i]*mult; 
		AInv[Arow][i]=AInv[Arow][i]-AInv[subrow][i]*mult; 
	}
	return 0;
}

int LSRMath::DivRow( vector<vector<double>>& Ain, vector<vector<double>>& AInv, int Arow, int divcol)
{ 
	int Asize = (int) Ain.size();
	double mult = 1/Ain[Arow][divcol];
	for (int i = 0; i<Asize; i++) 
	{
		Ain[Arow][i]=Ain[Arow][i]*mult; 
		AInv[Arow][i]=AInv[Arow][i]*mult;  
	}
	return 0;
}

int LSRMath::InvertMatrix( vector<vector<double>> Ain, vector<vector<double>>& AInv)
{
	int Asize = (int) Ain.size();
	for( int i = 0; i < Asize; i++ )
		if ( Ain[i].size() != Asize ) { return -1; }
	vector<vector<double>> Aarr( Asize, vector<double>( Asize ) );
	AInv.resize(Asize, vector<double>( Asize ));
	for (int i = 0; i<Asize; i++)
	{	
		for (int j = 0; j<Asize; j++)
		{
			if (i==j){	AInv[i][j]=1; }
			else {AInv[i][j]=0;}
			Aarr[i][j]=Ain[i][j];
		}
	}

	for (int i = 0; i < Asize - 1; i++)
		for (int j = i + 1; j < Asize; j++)
			SubRowMult(Aarr, AInv,j,i,i);
	for (int i = Asize - 1; i > 0; i--)
		for (int j = i - 1; j >= 0; j--)
			SubRowMult(Aarr, AInv,j,i,i);

	for (int i = 0; i < Asize; i++)
		DivRow(Aarr, AInv, i, i);
	return 0;
}

int LSRMath::TransposeMatrix( vector<vector<double>> Aarr, vector<vector<double>>& Atarr)
{
	int Xsize=(int) Aarr.size();
	int Ysize=(int) Aarr[0].size();
	for (int i=1; i<Xsize; i++)
		if ( Aarr[i].size() != Ysize ) { return -1; }
	Atarr.resize(Ysize, vector<double>( Xsize ));
	for (int i=0; i<Xsize; i++)
		for (int j=0; j<Ysize; j++)
			{ Atarr[j][i]=Aarr[i][j]; }
	return 0;
}

int LSRMath::MultiplyMatrix( vector<vector<double>> Aarr, vector<vector<double>> Barr, vector<vector<double>>& Carr)
{
	int Xsize=(int) Aarr.size();
	int Dimsize=(int) Aarr[0].size();
	for (int i=1; i<Xsize; i++)
		if ( Aarr[i].size() != Dimsize ) { return -1; }
	if ( Barr.size() != Dimsize ) { return -1; }
	int Ysize=(int) Barr[0].size();
	for (int i=1; i<Dimsize; i++)
		if ( (int) Barr[i].size() != Ysize ) { return -1; }

	Carr.resize(Xsize, vector<double>( Ysize ));

	for (int i = 0; i < Xsize; i++ )
	{
		for (int j = 0; j < Ysize; j++ )
		{
			Carr[i][j]=0;
			for (int k = 0; k < Dimsize; k++ )
			{ Carr[i][j]+=Aarr[i][k]*Barr[k][j]; }
		}
	}
	return 0;
}

int LSRMath::CalculateLSRError( vector<vector<double>> Aarr, vector<vector<double>> Barr, vector<vector<double>> solveVar, char* errorFile )
{
	vector<vector<double>> EstBarr;
	if (!MultiplyMatrix(Aarr,solveVar,EstBarr)==0) { return -1; }
	int XSize = (int) Barr.size();
	int YSize = (int) Barr[0].size();
	int solveVarXSize = (int) solveVar.size();
	
	FILE * pFile;
	pFile = fopen(errorFile,"w");

	fprintf(pFile,"This is the solution for A*q=B.\n\n\n");

	fprintf(pFile,"A Matrix:\n");
	for (int i = 0; i<XSize; i++)
	{
		for (int j = 0; j<solveVarXSize; j++)
		{ fprintf(pFile,"%f,\t",Aarr[i][j]); }
		fprintf(pFile,"\n");
	}


	fprintf(pFile,"\n\n");

	fprintf(pFile,"Solution Matrix (q):\n");
	for (int i = 0; i<solveVarXSize; i++)
	{
		for (int j = 0; j<YSize; j++)
		{ fprintf(pFile,"%f,\t",solveVar[i][j]); }
		fprintf(pFile,"\n");
	}



	fprintf(pFile,"\n\n");


	for (int i = 0; i<YSize; i++)
	{
		double sqrErr = 0;
		for (int j = 0; j<XSize; j++)
		{
			double realVal=Barr[j][i];
			double estVal=EstBarr[j][i];
			double errorVal=estVal-realVal;
			fprintf(pFile,"Real B: %f,\t",realVal);
			fprintf(pFile,"Estimated B: %f,\t",estVal);
			fprintf(pFile,"Error: %f,\t",errorVal);
			fprintf(pFile,"Percent Error: %f,\t",100*errorVal/realVal);
			sqrErr += (errorVal)*(errorVal);
			fprintf(pFile,"\n");
		}
			fprintf(pFile,"\nRoot Mean Square Error: %f\n\n\n",sqrt(sqrErr/XSize));
	}
			fclose(pFile);
	return 0;
}


int LSRMath::SolveLSR( vector<vector<double>> Aarr,
					  vector<vector<double>> Barr,
					  vector<vector<double>>& solveVar,
					  char* errorFile)
{
	vector<vector<double>> At;
	vector<vector<double>> AtA;
	vector<vector<double>> AtAInv;
	vector<vector<double>> AtB;
	
	if (Aarr.size()<Aarr[0].size()) { return -1; }

	if (!TransposeMatrix(Aarr,At)==0) { return -1; }
	if (!MultiplyMatrix(At,Aarr,AtA)==0) { return -1; }
	if (!MultiplyMatrix(At,Barr,AtB)==0) { return -1; }
	if (!InvertMatrix(AtA,AtAInv)==0) { return -1; }
	if (!MultiplyMatrix(AtAInv,AtB,solveVar)==0) { return -1; }
	if ( errorFile!="")
	{
		if (!CalculateLSRError( Aarr, Barr, solveVar, errorFile )==0) { return -1; }
	}
	return 0;

}

int LSRMath::SolveParabolicXYZt(vector<vector<double>> XYZarr, vector<double> Tarr, vector<vector<double>>& solveVar)
{
	int TSize=(int) Tarr.size();
	vector<vector<double>> Aarr( TSize, vector<double>( 3 ) );

	for ( int i = 0; i<TSize; i++ )
	{
		double t = Tarr[i];
		Aarr[i][0]=t*t;
		Aarr[i][1]=t;
		Aarr[i][2]=1;
	}
	
	if (!SolveLSR(Aarr,XYZarr,solveVar,"ParabolaPoints.txt")==0) { return -1; }
	return 0;
}

int LSRMath::findImgCoordsFromWorld(vector<vector<double>> coords, vector<vector<double>> M, vector<vector<double>>& imCoords)
{
	vector<vector<double>> imwCoords;
	if (MMatrixTransform( coords, M, imwCoords)!=0) {return -1;}
	imCoords.resize(2, vector<double>( 1 ));
	imCoords[0][0]=imwCoords[0][0]/imwCoords[2][0];
	imCoords[1][0]=imwCoords[1][0]/imwCoords[2][0];
	return 0;
}

int LSRMath::findAxesPoints(
							vector<vector<vector<vector<double>>>>& XYZcoords,
							double length
							 )
{
	
	XYZcoords.resize(
		3,vector<vector<vector<double>>>(
		2, vector<vector<double>>(
		3, vector<double>(
		1))));

	for (int i = 0; i<3; i++)
		for (int j = 0; j<3; j++)
			{ XYZcoords[i][0][j][0]=0; }

	for (int i = 0; i<3; i++)
		for (int j = 0; j<3; j++)
		{	
			if(i==j){ XYZcoords[i][1][j][0]=length; }
			else{ XYZcoords[i][1][j][0]=0; }
		}


	return 0;
}


int LSRMath::findImageAxesPoints(vector<vector<vector<vector<double>>>> XYZcoords, vector<vector<double>> M, vector<vector<vector<vector<double>>>>& ImCoords )
{
	int A = (int)XYZcoords.size();
	ImCoords.resize(A,vector<vector<vector<double>>>(2));
	for (int i = 0; i<A; i++)
		for (int j = 0; j<2; j++)
			if (findImgCoordsFromWorld( XYZcoords[i][j], M, ImCoords[i][j])!=0) {return -1;}
	return 0;
}

int LSRMath::MMatrixTransform(vector<vector<double>> coords, vector<vector<double>> M, vector<vector<double>>& Mcoords)
{
	vector<vector<double>> CoordsAppend1(4, vector<double>(1));
	CoordsAppend1[0][0]=coords[0][0];
	CoordsAppend1[1][0]=coords[1][0];
	CoordsAppend1[2][0]=coords[2][0];
	CoordsAppend1[3][0]=1;
	MultiplyMatrix(M,CoordsAppend1,Mcoords);
	return 0;
}

int LSRMath::findMMatrixFromLaserAxes(vector<vector<double>> vLaserX, vector<vector<double>> cLaserX, vector<vector<double>> vLaserY, vector<vector<double>> cLaserY, vector<vector<double>>& M)
{
	vector<vector<double>> zAxis;
	vector<vector<double>> yAxis;
	vector<vector<double>> centerPoint (3, vector<double>(1));
	if (CrossProduct( vLaserX, vLaserY, zAxis)!=0) {return -1;}
	if (CrossProduct( zAxis, vLaserX, yAxis)!=0) {return -1;}
	if (Normalize( vLaserX )!=0) {return -1;}
	if (Normalize( yAxis )!=0) {return -1;}
	if (Normalize( zAxis )!=0) {return -1;}
	M.resize(3, vector<double>(4));
	for (int i = 0; i<3; i++)
	{centerPoint[i][0] = (cLaserX[i][0] + cLaserY[i][0])/2;} 
	// While this center point is a few inches off each axis due to the structure of our robot, we have to make do.
	// It is a point inside our robot which is the important thing.
	for (int i = 0; i<3; i++)
	{
		M[0][i]=vLaserX[i][0];
		M[1][i]=yAxis[i][0];
		M[2][i]=zAxis[i][0];
		M[3][i]=0;
	}
	vector<vector<double>> NegCenterRot;
	if (MMatrixTransform( centerPoint, M, NegCenterRot)!=0) {return -1;}
	for (int i = 0; i<3; i++)
	{ M[3][i]=-1*NegCenterRot[i][0]; }
	return 0;
}


int LSRMath::findParabolicPoint(vector<vector<double>> SolveVar, double t, vector<vector<double>>& coords)
{
	coords.resize(3, vector<double>( 1 ));
	coords[0][0] = SolveVar[0][0]*t*t + SolveVar[1][0]*t+SolveVar[2][0];
	coords[1][0] = SolveVar[0][1]*t*t + SolveVar[1][1]*t+SolveVar[2][1];
	coords[2][0] = SolveVar[0][2]*t*t + SolveVar[1][2]*t+SolveVar[2][2];
	return 0;
}

int LSRMath::CrossProduct(vector<vector<double>> A, vector<vector<double>> B, vector<vector<double>>& AcB)
{
	AcB.resize(3, vector<double>( 1 ));
	AcB[0][0] = A[1][0]*B[2][0] - A[2][0]*B[1][0];
	AcB[1][0] = A[2][0]*B[0][0] - A[0][0]*B[2][0];
	AcB[2][0] = A[0][0]*B[1][0] - A[1][0]*B[0][0];
	return 0;
}



int LSRMath::Normalize(vector<vector<double>>& A)
{
	int XSize=(int) A.size();
	int YSize=(int) A[0].size();
	vector<double> VectorLengths;
	if(FindLengths(A, VectorLengths )!=0){return -1;}

	for (int j = 0; j<YSize; j++)
		for (int i = 0; i<XSize; i++)
		{ A[i][j] = A[i][j]/VectorLengths[j]; }  
	return 0;
}

int LSRMath::FindLengths(vector<vector<double>> A,  vector<double>& VectorLengths )
{
	int XSize=(int) A.size();
	int YSize=(int) A[0].size();
	for (int i = 1; i<XSize; i++)
		if (YSize!=A[i].size())
			{ return -1; }
	VectorLengths.resize(YSize);
	for (int j = 0; j<YSize; j++)
	{
		double sqrsum=0;
		for (int i = 0; i<XSize; i++)
		{ double x = A[i][j];
			sqrsum += x*x; }
		VectorLengths[j] = pow(sqrsum,.5);
	}
	return 0;
}

int LSRMath::DotProduct(vector<vector<double>> A, vector<vector<double>> B, double& AdB)
{
	vector<vector<double>> At;
	vector<vector<double>> BAt;
	if (TransposeMatrix(A,At)!=0){return -1;}
	if (MultiplyMatrix(At,B,BAt)!=0){return -1;}
	if (BAt.size()!=1){return -1;}
	if (BAt[0].size()!=1){return -1;}
	AdB=BAt[0][0];
	return 0;
}


int LSRMath::FindRotationAngles( vector<vector<double>> MExt1, vector<double>& RotXYZ1 ) 
{
	
	RotXYZ1.resize(3);

	RotXYZ1[1] = asin( -1 * MExt1[2][0] );

	RotXYZ1[0] = asin( MExt1[1][0] / cos( RotXYZ1[1] ) );
	if ( MExt1[0][0] / cos( RotXYZ1[1] ) < 0  )
	{ RotXYZ1[0]= M_PI - RotXYZ1[0]	;}

	RotXYZ1[2] = asin( MExt1[2][1] / cos( RotXYZ1[1] ) );
	if ( MExt1[2][2] / cos( RotXYZ1[1] ) < 0  )
	{ RotXYZ1[2]= M_PI - RotXYZ1[2]	;}
	
	double M01Test = cos(RotXYZ1[0])*sin(RotXYZ1[1])*sin(RotXYZ1[2]) - sin(RotXYZ1[0])*cos(RotXYZ1[2]) ;
	double M11Test = sin(RotXYZ1[0])*sin(RotXYZ1[1])*sin(RotXYZ1[2]) + cos(RotXYZ1[0])*cos(RotXYZ1[2]);
	double M02Test = cos(RotXYZ1[0])*sin(RotXYZ1[1])*cos(RotXYZ1[2]) + sin(RotXYZ1[0])*sin(RotXYZ1[2]);
	double M12Test = sin(RotXYZ1[0])*sin(RotXYZ1[1])*cos(RotXYZ1[2]) - cos(RotXYZ1[0])*sin(RotXYZ1[2]);
	if ( (M01Test*MExt1[0][1]<0) ||
		(M02Test*MExt1[0][2]<0) ||
		(M11Test*MExt1[1][1]<0) ||
		(M12Test*MExt1[1][2]<0) )
	{
		RotXYZ1[1] = M_PI - RotXYZ1[1];

		RotXYZ1[0] = asin( MExt1[1][0] / cos( RotXYZ1[1] ) );
		if ( MExt1[0][0] / cos( RotXYZ1[1] ) < 0 )
		{ RotXYZ1[0]= M_PI - RotXYZ1[0]	;}

		RotXYZ1[2] = asin( MExt1[2][1] / cos( RotXYZ1[1] ) );
		if ( MExt1[2][2] / cos( RotXYZ1[1] ) < 0 )
		{ RotXYZ1[2]= M_PI - RotXYZ1[2]	;}

		M01Test = cos(RotXYZ1[0])*sin(RotXYZ1[1])*sin(RotXYZ1[2]) - sin(RotXYZ1[0])*cos(RotXYZ1[2]);
		M02Test = sin(RotXYZ1[0])*sin(RotXYZ1[1])*sin(RotXYZ1[2]) + cos(RotXYZ1[0])*cos(RotXYZ1[2]);
		M11Test = cos(RotXYZ1[0])*sin(RotXYZ1[1])*cos(RotXYZ1[2]) + sin(RotXYZ1[0])*sin(RotXYZ1[2]);
		M12Test = sin(RotXYZ1[0])*sin(RotXYZ1[1])*cos(RotXYZ1[2]) - cos(RotXYZ1[0])*sin(RotXYZ1[2]);

	}
	
	if ( (M01Test*MExt1[0][1]<0) ||
		(M02Test*MExt1[0][2]<0) ||
		(M11Test*MExt1[1][1]<0) ||
		(M12Test*MExt1[1][2]<0) )
	{return -1;}
	for (int i=0; i<3; i++){RotXYZ1[i]=RotXYZ1[i]*180/M_PI;}

	return 0;
}


LSRMath::LSRMath() { }
LSRMath::~LSRMath() { }
