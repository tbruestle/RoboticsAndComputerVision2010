#pragma once
using namespace std;

class OpenGLDraw{

protected:





public:
	int OpenGLDraw::DrawAxes( 
			int argc,
			char** argv,
			vector<vector<double>> M1,
			vector<vector<double>> M2
		);
	int OpenGLDraw::DrawLasers( 
			int argc,
			char** argv,
			vector<vector<double>> M1,
			vector<vector<double>> M2,
			vector<vector<vector<vector<double>>>> LaserCoords
		);
	int OpenGLDraw::DrawLaserAxes( 
			int argc, 
			char** argv,  
			vector<vector<double>> MExt1, 
			vector<vector<double>> MExt2, 
			vector<vector<double>> MInt1, 
			vector<vector<double>> MInt2, 
			vector<vector<double>> vLaserX, 
			vector<vector<double>> vLaserY
		);
	//int OpenGLDraw::DrawLaserAxes( int argc, char** argv, vector<vector<double>> MRobot);
	//void OpenGLDraw::myKeyboardFunc( unsigned char key, int x, int y );
	//void OpenGLDraw::drawScene(void);
	void OpenGLDraw::initRendering();
	//void OpenGLDraw::resizeWindow(int w, int h);
	int OpenGLDraw::OpenGLTest( int argc, char** argv );
	int OpenGLDraw::SetFrame( int w, int h );
	int OpenGLDraw::DrawBallTrajectory(  
		int argc, 
		char** argv, 
		vector<vector<double>> M1, 
		vector<vector<double>> M2,  
		vector<vector<double>> MExt1, 
		vector<vector<double>> MExt2,
		vector<vector<double>> MInt1,
		vector<vector<double>> MInt2,  
		vector<vector<double>> XYZarr,  
		vector<double> Tarr,
		vector<vector<double>> solveVar
		);
	OpenGLDraw();
	~OpenGLDraw();
};