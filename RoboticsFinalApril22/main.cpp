#include <math.h>
#include <cv.h>
#include <ctype.h>
#include <string.h>
#include <cvaux.h>
#include <vector>
#include <iterator>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <highgui.h>
#include <iostream>
#include <windows.h>
#include <conio.h>
#include "stdafx.h"
#include "nxt_remote.h"
#include "LSRMath.h"
#include "OpenGLDraw.h"
#include "openglut.h" // OpenGL Graphics Utility Library 
using namespace std;

#define M_PI       3.14159265358979323846
int initNxt(nxt_remote *nxtr, char* comPort)
{

	printf("Starting NXT communication on port %s... \n", comPort);
  if (nxtr->startcommunication(comPort)==0){ //initialize interface and check if initialization failed
    _cprintf("NXT communication failed.");
    while (!_kbhit()); //wait for keypress
    return -1;
  }
  return 0;
}



int stopNxt(nxt_remote *nxtr)
{
	nxtr->stopcommunication();   //stop interfacing. This also stops the motors.
	return 0;
}
int runRobot(double RotationAngle, double BowAngle, bool isReturnBow, nxt_remote *nxtr){

	nxtr->NXT_motoron[0]=1;
	nxtr->NXT_motoron[1]=1;
	nxtr->NXT_motoron[2]=1;
 // while((!_kbhit())){

	double rotSlope = .1808;
	double rotInit = 2.5173;
	double bowSlope;
	double bowInit;

	if (isReturnBow == false)
	{
		//8 -> 90 degrees
		bowSlope = (8./90.);
		bowInit = 0;
	}
	else
	{
		//-9.2 -> -90 degrees
		bowSlope = 9.2/90.;
		bowInit = 0;
	}


	double theta = RotationAngle;
	double phi = BowAngle;
	while (theta>180){theta=theta-360;}
	while (theta<-180){theta=theta+360;}
	int c1=7;
	int c2=28;
	if (theta>0)
	{
		int tireSpeed = (int) (rotSlope*theta + rotInit +.5);
		// This equation was found by LSR of the actual rotations
		// associated with the angle.
		// The added .5 is for round to nearest
		nxtr->NXT_motorval[0]=tireSpeed;
		nxtr->NXT_motorval[2]=-tireSpeed;
	}
	else if (theta<0)
	{
		int tireSpeed = (int) (-1*rotSlope*theta + rotInit +.5);
		nxtr->NXT_motorval[0]=-tireSpeed;
		nxtr->NXT_motorval[2]=tireSpeed;
	}
	else if (theta==0)
	{
		nxtr->NXT_motorval[0]=0;
		nxtr->NXT_motorval[2]=0;
	}

	if (phi==0)
	{
		int armSpeed = 0;
		// This equation was found by LSR of the actual rotations
		// associated with the angle.
		// The added .5 is for round to nearest
		nxtr->NXT_motorval[1]=armSpeed;
	}
	else if (isReturnBow == false)
	{
		int armSpeed = (int) (bowSlope*(phi+90) + bowInit +.5);
		// This equation was found by LSR of the actual rotations
		// associated with the angle.
		// The added .5 is for round to nearest
		nxtr->NXT_motorval[1]=armSpeed;
	}
	else
	{
		int armSpeed = (int) (bowSlope*(phi+90) + bowInit +.5);
		// This equation was found by LSR of the actual rotations
		// associated with the angle.
		// The added .5 is for round to nearest
		nxtr->NXT_motorval[1]=-armSpeed;
	}


/*
	for(int i = 0; i<4; i++)
	{
		nxtr->NXT_motorval[0]=50;
		nxtr->NXT_motorval[1]=50;
		nxtr->NXT_motorval[2]=-50;
    Sleep(2000); 
		nxtr->NXT_motorval[0]=50;
		nxtr->NXT_motorval[1]=-50;
		nxtr->NXT_motorval[2]=-50;
    Sleep(2000); 
		nxtr->NXT_motorval[0]=-50;
		nxtr->NXT_motorval[1]=-50;
		nxtr->NXT_motorval[2]=50;
    Sleep(2000); 
		nxtr->NXT_motorval[0]=-50;
		nxtr->NXT_motorval[1]=50;
		nxtr->NXT_motorval[2]=50;
    Sleep(2000); 
	}

*/
    Sleep(1000);
    nxtr->NXT_motorval[0]=0;
    nxtr->NXT_motorval[1]=0;
    nxtr->NXT_motorval[2]=0;
    Sleep(50); //this is important so that the NXT-managing background thread is not blocked
 // }
	return 0;

  }


int findMidpoint( vector<double>& ppAvg1, vector<vector<double>> pointPairs, double midXobj, double midYobj, vector<vector<double>>& solveVar )
{
	int ppCount = (int)pointPairs[0].size();
	if ( ppCount < 3) {return -1;}

	LSRMath *lsr1;
	lsr1=new LSRMath(); 

	vector<vector<double>> Amatrix(ppCount*2, vector<double>(6));
	vector<vector<double>> Bmatrix(ppCount*2, vector<double>(1));
	for (int j = 0; j<4; j++) {ppAvg1[j]=0;}

	for( int i = 0; i < ppCount*2; i += 2)
	{
		double xObj=pointPairs[0][i/2];
		double yObj=pointPairs[1][i/2];
		double xIm=pointPairs[2][i/2];
		double yIm=pointPairs[3][i/2];

		Amatrix[i][0]=xObj;
		Amatrix[i][1]=yObj;
		Amatrix[i][2]=1;
		for (int j = 3; j < 6; j++){ Amatrix[i][j]=0; }
		Bmatrix[i][0]=xIm;
		
		for (int j = 0; j < 3; j++){ Amatrix[i+1][j]=0; }
		Amatrix[i+1][3]=xObj;
		Amatrix[i+1][4]=yObj;
		Amatrix[i+1][5]=1;
		Bmatrix[i+1][0]=yIm;
	}	
		if (!lsr1->SolveLSR(Amatrix,Bmatrix,solveVar,"")==0) { return -1; }

		ppAvg1[0]= midXobj;
		ppAvg1[1]= midYobj;
		ppAvg1[2]= midXobj*solveVar[0][0] + midYobj*solveVar[1][0] + solveVar[2][0];
		ppAvg1[3]= midXobj*solveVar[3][0] + midYobj*solveVar[4][0] + solveVar[5][0];



	return 0;
}


int findRansacMidpoint( vector<double>& ppAvg1, vector<vector<double>> pointPairs, double midXobj, double midYobj )
{ 
		srand((int) time(0));  /*initialize random number generator*/ 

		int ppCount = (int)pointPairs[0].size();
		vector<vector<double>> solveVar;

		vector<vector<double>> pointPairsX (4,vector<double>(0));

		int k = 0;
		double scale1;
		double scale2;
		double stretch = 0;
		double maxStretchRatio = 2.;
		cout << "X/Y Scale Ratio: ";
		while(stretch>maxStretchRatio || stretch<(1./maxStretchRatio))
		{
			if (k>0 && ppCount == 5) {return -1;}
			if (k>5 && ppCount == 6) {return -1;}
			if (k>10) {return -1;}
			pointPairsX[0].resize(0);
			pointPairsX[1].resize(0);
			pointPairsX[2].resize(0);
			pointPairsX[3].resize(0);
			while (findMidpoint( ppAvg1, pointPairsX, midXobj, midYobj, solveVar )!=0)
			{
				pointPairsX[0].resize(0);
				pointPairsX[1].resize(0);
				pointPairsX[2].resize(0);
				pointPairsX[3].resize(0);
				double RandMax;
				if ( ppCount < 8 ) {RandMax = 5./(double)ppCount;}
				else {RandMax = ((double)ppCount-3.)/(double)ppCount;}
				for( int i = 0; i < ppCount; i++ )	
					if (((1./2520.)*(double)(rand()%2520))<RandMax)
						for (int j = 0; j <4; j++)
							{ pointPairsX[j].push_back(pointPairs[j][i]); } 
			}
			double scale1 = solveVar[0][0]*solveVar[0][0] + solveVar[1][0]*solveVar[1][0];
			double scale2 = solveVar[3][0]*solveVar[3][0] + solveVar[4][0]*solveVar[4][0];
			stretch = scale1/scale2;
			if (stretch<0){stretch = -1*stretch;}
			cout << stretch << " ";
			
			k++;
		}
		return 0;
}




/*
 * A Demo to OpenCV Implementation of SURF
 * Further Information Refer to "SURF: Speed-Up Robust Feature"
 * Author: Liu Liu
 * liuliu.1987+opencv@gmail.com
 */

// define whether to use approximate nearest-neighbor search
#define USE_FLANN

IplImage *image = 0;

double compareSURFDescriptors( const float* d1, const float* d2, double best, int length )
{
    double total_cost = 0;
    assert( length % 4 == 0 );
    for( int i = 0; i < length; i += 4 )
    {
        double t0 = d1[i] - d2[i];
        double t1 = d1[i+1] - d2[i+1];
        double t2 = d1[i+2] - d2[i+2];
        double t3 = d1[i+3] - d2[i+3];
        total_cost += t0*t0 + t1*t1 + t2*t2 + t3*t3;
        if( total_cost > best )
            break;
    }
    return total_cost;
}


int naiveNearestNeighbor( const float* vec, int laplacian,
                      const CvSeq* model_keypoints,
                      const CvSeq* model_descriptors )
{
    int length = (int)(model_descriptors->elem_size/sizeof(float));
    int i, neighbor = -1;
    double d, dist1 = 1e6, dist2 = 1e6;
    CvSeqReader reader, kreader;
    cvStartReadSeq( model_keypoints, &kreader, 0 );
    cvStartReadSeq( model_descriptors, &reader, 0 );

    for( i = 0; i < model_descriptors->total; i++ )
    {
        const CvSURFPoint* kp = (const CvSURFPoint*)kreader.ptr;
        const float* mvec = (const float*)reader.ptr;
    	CV_NEXT_SEQ_ELEM( kreader.seq->elem_size, kreader );
        CV_NEXT_SEQ_ELEM( reader.seq->elem_size, reader );
        if( laplacian != kp->laplacian )
            continue;
        d = compareSURFDescriptors( vec, mvec, dist2, length );
        if( d < dist1 )
        {
            dist2 = dist1;
            dist1 = d;
            neighbor = i;
        }
        else if ( d < dist2 )
            dist2 = d;
    }
    if ( dist1 < 0.6*dist2 )
        return neighbor;
    return -1;
}

void findPairs( const CvSeq* objectKeypoints, const CvSeq* objectDescriptors,
           const CvSeq* imageKeypoints, const CvSeq* imageDescriptors, vector<int>& ptpairs )
{
    int i;
    CvSeqReader reader, kreader;
    cvStartReadSeq( objectKeypoints, &kreader );
    cvStartReadSeq( objectDescriptors, &reader );
    ptpairs.clear();

    for( i = 0; i < objectDescriptors->total; i++ )
    {
        const CvSURFPoint* kp = (const CvSURFPoint*)kreader.ptr;
        const float* descriptor = (const float*)reader.ptr;
        CV_NEXT_SEQ_ELEM( kreader.seq->elem_size, kreader );
        CV_NEXT_SEQ_ELEM( reader.seq->elem_size, reader );
        int nearest_neighbor = naiveNearestNeighbor( descriptor, kp->laplacian, imageKeypoints, imageDescriptors );
        if( nearest_neighbor >= 0 )
        {
            ptpairs.push_back(i);
            ptpairs.push_back(nearest_neighbor);
        }
    }
}


void flannFindPairs( const CvSeq*, const CvSeq* objectDescriptors,
           const CvSeq*, const CvSeq* imageDescriptors, vector<int>& ptpairs )
{
	int length = (int)(objectDescriptors->elem_size/sizeof(float));

    cv::Mat m_object(objectDescriptors->total, length, CV_32F);
	cv::Mat m_image(imageDescriptors->total, length, CV_32F);


	// copy descriptors
    CvSeqReader obj_reader;
	float* obj_ptr = m_object.ptr<float>(0);
    cvStartReadSeq( objectDescriptors, &obj_reader );
    for(int i = 0; i < objectDescriptors->total; i++ )
    {
        const float* descriptor = (const float*)obj_reader.ptr;
        CV_NEXT_SEQ_ELEM( obj_reader.seq->elem_size, obj_reader );
        memcpy(obj_ptr, descriptor, length*sizeof(float));
        obj_ptr += length;
    }
    CvSeqReader img_reader;
	float* img_ptr = m_image.ptr<float>(0);
    cvStartReadSeq( imageDescriptors, &img_reader );
    for(int i = 0; i < imageDescriptors->total; i++ )
    {
        const float* descriptor = (const float*)img_reader.ptr;
        CV_NEXT_SEQ_ELEM( img_reader.seq->elem_size, img_reader );
        memcpy(img_ptr, descriptor, length*sizeof(float));
        img_ptr += length;
    }

    // find nearest neighbors using FLANN
    cv::Mat m_indices(objectDescriptors->total, 2, CV_32S);
    cv::Mat m_dists(objectDescriptors->total, 2, CV_32F);
    cv::flann::Index flann_index(m_image, cv::flann::KDTreeIndexParams(4));  // using 4 randomized kdtrees
    flann_index.knnSearch(m_object, m_indices, m_dists, 2, cv::flann::SearchParams(64) ); // maximum number of leafs checked

    int* indices_ptr = m_indices.ptr<int>(0);
    float* dists_ptr = m_dists.ptr<float>(0);
    for (int i=0;i<m_indices.rows;++i) {
    	if (dists_ptr[2*i]<0.6*dists_ptr[2*i+1]) {
    		ptpairs.push_back(i);
    		ptpairs.push_back(indices_ptr[2*i]);
    	}
    }
}


/* a rough implementation for object location */
int locatePlanarObject( const CvSeq* objectKeypoints, const CvSeq* objectDescriptors,
                    const CvSeq* imageKeypoints, const CvSeq* imageDescriptors,
                    const CvPoint src_corners[4], CvPoint dst_corners[4] )
{
    double h[9];
    CvMat _h = cvMat(3, 3, CV_64F, h);
    vector<int> ptpairs;
    vector<CvPoint2D32f> pt1, pt2;
    CvMat _pt1, _pt2;
    int i, n;

#ifdef USE_FLANN
    flannFindPairs( objectKeypoints, objectDescriptors, imageKeypoints, imageDescriptors, ptpairs );
#else
    findPairs( objectKeypoints, objectDescriptors, imageKeypoints, imageDescriptors, ptpairs );
#endif

    n = (int)ptpairs.size()/2;
    if( n < 4 )
        return 0;

    pt1.resize(n);
    pt2.resize(n);
    for( i = 0; i < n; i++ )
    {
        pt1[i] = ((CvSURFPoint*)cvGetSeqElem(objectKeypoints,ptpairs[i*2]))->pt;
        pt2[i] = ((CvSURFPoint*)cvGetSeqElem(imageKeypoints,ptpairs[i*2+1]))->pt;
    }

    _pt1 = cvMat(1, n, CV_32FC2, &pt1[0] );
    _pt2 = cvMat(1, n, CV_32FC2, &pt2[0] );
    if( !cvFindHomography( &_pt1, &_pt2, &_h, CV_RANSAC, 5 ))
        return 0;

    for( i = 0; i < 4; i++ )
    {
        double x = src_corners[i].x, y = src_corners[i].y;
        double Z = 1./(h[6]*x + h[7]*y + h[8]);
        double X = (h[0]*x + h[1]*y + h[2])*Z;
        double Y = (h[3]*x + h[4]*y + h[5])*Z;
        dst_corners[i] = cvPoint(cvRound(X), cvRound(Y));
    }

    return 1;
}

int doSift(char filename1[50],char filename2[50], vector<double>& midPair,  vector<vector<double>> pointPairs, double midXobj, double midYobj, 
	bool showPic = true)
{
	midPair.resize(4);
//We used the given code but made serious changes to this file to return what we wanted.
  pointPairs.resize(4);

	const char* object_filename = filename1;
    const char* scene_filename = filename2;

    CvMemStorage* storage = cvCreateMemStorage(0);

//    cvNamedWindow("Object", 1);

    static CvScalar colors[] = 
    {
        {{0,0,255}},
        {{0,128,255}},
        {{0,255,255}},
        {{0,255,0}},
        {{255,128,0}},
        {{255,255,0}},
        {{255,0,0}},
        {{255,0,255}},
        {{255,255,255}}
    };

    IplImage* object = cvLoadImage( object_filename, CV_LOAD_IMAGE_GRAYSCALE);
    IplImage* image = cvLoadImage( scene_filename, CV_LOAD_IMAGE_GRAYSCALE );
    if( !object || !image )
    {
        fprintf( stderr, "Can not load %s and/or %s\n"
            "Usage: find_obj [<object_filename> <scene_filename>]\n",
            object_filename, scene_filename );
        exit(-1);
    }
  //  IplImage* object_color = cvCreateImage(cvGetSize(object), 8, 3);
 ///   cvCvtColor( object, object_color, CV_GRAY2BGR );
    
    CvSeq *objectKeypoints = 0, *objectDescriptors = 0;
    CvSeq *imageKeypoints = 0, *imageDescriptors = 0;
    int i;
    CvSURFParams params = cvSURFParams(500, 1);

    double tt = (double)cvGetTickCount();
    cvExtractSURF( object, 0, &objectKeypoints, &objectDescriptors, storage, params );
    cvExtractSURF( image, 0, &imageKeypoints, &imageDescriptors, storage, params );
    tt = (double)cvGetTickCount() - tt;
//    printf("Object Descriptors: %d\n", objectDescriptors->total);
//    printf("Image Descriptors: %d\n", imageDescriptors->total);
 //   printf( "Extraction time = %gms\n", tt/(cvGetTickFrequency()*1000.));
 //   CvPoint src_corners[4] = {{0,0}, {object->width,0}, {object->width, object->height}, {0, object->height}};
 //   CvPoint dst_corners[4];
   
	IplImage* correspond = cvCreateImage( cvSize(image->width, object->height+image->height), 8, 1 );
    cvSetImageROI( correspond, cvRect( 0, 0, object->width, object->height ) );
    cvCopy( object, correspond );
    cvSetImageROI( correspond, cvRect( 0, object->height, correspond->width, correspond->height ) );
    cvCopy( image, correspond );
    cvResetImageROI( correspond );

#ifdef USE_FLANN
    //printf("Using approximate nearest neighbor search\n");
#endif
/*
	int sumx1 = 0;
	int sumy1 = 0;
	int avex1 = 0;
	int avey1 = 0;
	int sumx2 = 0;
	int sumy2 = 0;
	int avex2 = 0;
	int avey2 = 0;

	if( locatePlanarObject( objectKeypoints, objectDescriptors, imageKeypoints,
        imageDescriptors, src_corners, dst_corners ))
    {
        for( i = 0; i < 4; i++ )
        {
            CvPoint r1 = dst_corners[i%4];
            CvPoint r2 = dst_corners[(i+1)%4];
			cvLine( correspond, cvPoint(r1.x, r1.y+object->height ),
			cvPoint(r2.x, r2.y+object->height ), colors[8] );
			//cout << r1.x << "\t" << r1.y << "\n";
			//printf("%d\t%d\t%d\t%d\n", r1.x, r1.y, r2.x, r2.y);
			sumx1 += r1.x;
			sumy1 += r1.y;
			sumx2 += r2.x;
			sumy2 += r2.y;
		}
		avex1 = sumx1/4;
		avey1 = sumy1/4;
		avex2 = sumx2/4;
		avey2 = sumy2/4;
		//printf("%d\t%d\n%d\t%d\n", avex1, avey1, avex2, avey2);
	}
*/
	vector<int> ptpairs;
#ifdef USE_FLANN
    flannFindPairs( objectKeypoints, objectDescriptors, imageKeypoints, imageDescriptors, ptpairs );
#else
    findPairs( objectKeypoints, objectDescriptors, imageKeypoints, imageDescriptors, ptpairs );
#endif
   for( i = 0; i < (int)ptpairs.size(); i += 2 )
    {
        CvSURFPoint* r1 = (CvSURFPoint*)cvGetSeqElem( objectKeypoints, ptpairs[i] );
        CvSURFPoint* r2 = (CvSURFPoint*)cvGetSeqElem( imageKeypoints, ptpairs[i+1] );
   //     cvLine( correspond, cvPointFrom32f(r1->pt),
   //        cvPoint(cvRound(r2->pt.x), cvRound(r2->pt.y+object->height)), colors[8] );
		
			pointPairs[0].push_back((double)r1->pt.x);
			pointPairs[1].push_back((double)r1->pt.y);
			pointPairs[2].push_back((double)r2->pt.x);
			pointPairs[3].push_back((double)r2->pt.y);
    }
    printf( "Found %d point pairs.\n", (int)pointPairs[0].size());
   
   if(((int)pointPairs[0].size())<5) { return -1; }
//    cvLine( correspond, cvPoint(ppAvg1[0], ppAvg1[1]), cvPoint(ppAvg1[2], ppAvg1[3]+object->height ), colors[8] );

/*    for( i = 0; i < objectKeypoints->total; i++ )
    {
        CvSURFPoint* r = (CvSURFPoint*)cvGetSeqElem( objectKeypoints, i );
        CvPoint center;
        int radius;
        center.x = cvRound(r->pt.x);
        center.y = cvRound(r->pt.y);
        radius = cvRound(r->size*1.2/9.*2);
        cvCircle( object_color, center, radius, colors[0], 1, 8, 0 );
    }
*/
//    cvShowImage( "Object", object_color );
//    cvDestroyWindow("Object");
	if(findRansacMidpoint( midPair, pointPairs, midXobj, midYobj )!=0){
		cout << "Failed RANSAC Test." << endl;
		return -1;
		}
	if (showPic)
	{
		
        cvLine( correspond, 
            cvPoint(cvRound(midPair[0]), cvRound(midPair[1])),
            cvPoint(cvRound(midPair[2]), cvRound(midPair[3]+object->height)), colors[7] );
		cvNamedWindow("Object Correspond", 1); 
		cvShowImage( "Object Correspond", correspond );
		cvWaitKey(0);
		cvDestroyWindow("Object Correspond");
	}

    cvDestroyWindow("Object SURF");

    return 0;

}





int takePicture(int firstPic, int lastPic, int cameraNumber)
{
  CvCapture * pCapture    = 0;
  IplImage *  pVideoFrame = 0;
  int         j;
  char        filename[50];

  // Initialize video capture

  //choose camera - We wrote this
	  pCapture = cvCreateCameraCapture( cameraNumber );

  if( !pCapture )
  {
    fprintf(stderr, "failed to initialize video capture\n");
    return -1;
  }

  // Capture three video frames and write them as files
  for(j=firstPic; j<=lastPic; j++)
  {
    pVideoFrame = cvQueryFrame( pCapture );
    if( !pVideoFrame )
    {
      fprintf(stderr, "failed to get a video frame\n");
    }

    // Write the captured video frame as an image file
	//choose camera - We wrote this
	   sprintf(filename, "Camera%dBall%d.jpg", cameraNumber, j);

    if( !cvSaveImage(filename, pVideoFrame) )
    {
      fprintf(stderr, "failed to write image file %s\n", filename);
    }

    // IMPORTANT: Don't release or modify the image returned
    // from cvQueryFrame() !
  }

  // Terminate video capture and free capture resources
  cvReleaseCapture( &pCapture );

  return 0;
}



int calibrateCameraPart2(
	vector<vector<vector<double>>>& MRots,
	vector<vector<double>>& MIntrinsic,
	vector<vector<double>>& MTrans,
	int board_n,
	int successes,
	CvMat* object_points,
	CvMat* image_points,
	CvMat* point_counts,
	CvPoint2D32f* corners,
	int corner_count,
	IplImage *image,
	IplImage *gray_image,
	CvCapture* capture
	)
{
	CvMat* intrinsic_matrix		= cvCreateMat( 3, 3, CV_32FC1 );
	CvMat* distortion_coeffs	= cvCreateMat( 5, 1, CV_32FC1 );

	// Allocate matrices according to how many chessboards found
	CvMat* object_points2 = cvCreateMat( successes*board_n, 3, CV_32FC1 );
	CvMat* image_points2 = cvCreateMat( successes*board_n, 2, CV_32FC1 );
	CvMat* point_counts2 = cvCreateMat( successes, 1, CV_32SC1 );
	
	// Transfer the points into the correct size matrices
	for( int i = 0; i < successes*board_n; ++i ){
		CV_MAT_ELEM( *image_points2, float, i, 0) = CV_MAT_ELEM( *image_points, float, i, 0 );
		CV_MAT_ELEM( *image_points2, float, i, 1) = CV_MAT_ELEM( *image_points, float, i, 1 );
		CV_MAT_ELEM( *object_points2, float, i, 0) = CV_MAT_ELEM( *object_points, float, i, 0 );
		CV_MAT_ELEM( *object_points2, float, i, 1) = CV_MAT_ELEM( *object_points, float, i, 1 );
		CV_MAT_ELEM( *object_points2, float, i, 2) = CV_MAT_ELEM( *object_points, float, i, 2 );
	}

	for( int i=0; i < successes; ++i ){
		CV_MAT_ELEM( *point_counts2, int, i, 0 ) = CV_MAT_ELEM( *point_counts, int, i, 0 );
	}

	// At this point we have all the chessboard corners we need
	// Initiliazie the intrinsic matrix such that the two focal lengths
	// have a ratio of 1.0

	CV_MAT_ELEM( *intrinsic_matrix, float, 0, 0 ) = 1.0;
	CV_MAT_ELEM( *intrinsic_matrix, float, 1, 1 ) = 1.0;

	// Calibrate the camera

	CvMat* rotation_vectors = cvCreateMat(successes, 3, CV_64FC1);
	CvMat* translation_vectors = cvCreateMat(successes, 3, CV_64FC1);

	cvCalibrateCamera2( object_points2, image_points2, point_counts2, cvGetSize( image ), 
		intrinsic_matrix, distortion_coeffs,
		rotation_vectors, translation_vectors, CV_CALIB_FIX_ASPECT_RATIO ); 
	char rMatFilename[32];
	MRots.resize(successes, vector<vector<double>> ( 3, vector<double> (3)));
	MIntrinsic.resize( 3, vector<double> (3));
	MTrans.resize(successes, vector<double> (3));
	
	for (int j = 0; j < 3; j++)
	{
		for (int k = 0; k < 3; k++)
		{	MIntrinsic[j][k]= (double)CV_MAT_ELEM( *intrinsic_matrix, float, j, k ); }  //May have to switch j & k in this line.
	}

/*
	// Save the intrinsics and distortions
	cvSave( "Intrinsics.xml", intrinsic_matrix );
	cvSave( "translation_vectors.xml", translation_vectors );
	cvSave( "rotation_vectors.xml", rotation_vectors );
	cvSave( "Distortion.xml", distortion_coeffs );
*/	
	for( int i = 0; i < successes; ++i ){
		CvMat* rotation_vector = cvCreateMat(3, 1, CV_64FC1);
		CvMat* rotation_matrix = cvCreateMat( 3, 3, CV_64FC1 );
		for (int j = 0; j < 3; j++)
		{ CV_MAT_ELEM( *rotation_vector, double, j, 0) = CV_MAT_ELEM( *rotation_vectors, double, i, j ); }
		cvRodrigues2(rotation_vector, rotation_matrix, NULL);
        sprintf( rMatFilename, "rotation_matrix%d.xml", i );
		cvSave( rMatFilename, rotation_matrix );
		for (int j = 0; j < 3; j++)
		{
			for (int k = 0; k < 3; k++)
			{MRots[i][j][k]= CV_MAT_ELEM( *rotation_matrix, double, j, k ); }  //May have to switch j & k in this line.
			MTrans[i][j]=(double)CV_MAT_ELEM( *translation_vectors, double, i, j );
		}
		
		cvReleaseMat( &rotation_vector ); 
		cvReleaseMat( &rotation_matrix ); 
	}
/*
	// Example of loading these matrices back in
	CvMat *intrinsic = (CvMat*)cvLoad( "Intrinsics.xml" );
	CvMat *distortion = (CvMat*)cvLoad( "Distortion.xml" );

	// Build the undistort map that we will use for all subsequent frames
	IplImage* mapx = cvCreateImage( cvGetSize( image ), IPL_DEPTH_32F, 1 );
	IplImage* mapy = cvCreateImage( cvGetSize( image ), IPL_DEPTH_32F, 1 );
	cvInitUndistortMap( intrinsic, distortion, mapx, mapy );
*/
	// Run the camera to the screen, now showing the raw and undistorted image
/*
	cvNamedWindow( "Undistort" );

	while( image ){
		IplImage *t = cvCloneImage( image );
		cvShowImage( "Calibration", image ); // Show raw image
		cvRemap( t, image, mapx, mapy ); // undistort image
		cvReleaseImage( &t );
		cvShowImage( "Undistort", image ); // Show corrected image

		// Handle pause/unpause and esc
		int c = cvWaitKey( 15 );
		if( c == 'p' ){
			c = 0;
			while( c != 'p' && c != 27 ){
				c = cvWaitKey( 250 );
			}
		}
		if( c == 27 )
			break;
		image = cvQueryFrame( capture );
	}
*/
	return 0;

}

int calibrateCamera(	vector<vector<vector<double>>>& MRotsA,
	vector<vector<double>>& MIntrinsicA,
	vector<vector<double>>& MTransA,
	vector<vector<vector<double>>>& MRotsB,
	vector<vector<double>>& MIntrinsicB,
	vector<vector<double>>& MTransB,
	int camera1,
	int camera2
)
{
	int n_boards = 0;
	const int board_dt = 20;
	int board_w;
	int board_h;
	board_w = 5; // Board width in squares
	board_h = 8; // Board height 
	n_boards = 8; // Number of boards
	int board_n = board_w * board_h;
	CvSize board_sz = cvSize( board_w, board_h );
	CvCapture* capture1 = cvCreateCameraCapture( camera1 );
	assert( capture1 );
	CvCapture* capture2 = cvCreateCameraCapture( camera2 );
	assert( capture2 );

	cvNamedWindow( "Calibration1" );
	cvNamedWindow( "Calibration2" );
	// Allocate Sotrage
	CvMat* image_points1		= cvCreateMat( n_boards*board_n, 2, CV_32FC1 );
	CvMat* object_points1		= cvCreateMat( n_boards*board_n, 3, CV_32FC1 );
	CvMat* point_counts1			= cvCreateMat( n_boards, 1, CV_32SC1 );

	CvMat* image_points2		= cvCreateMat( n_boards*board_n, 2, CV_32FC1 );
	CvMat* object_points2		= cvCreateMat( n_boards*board_n, 3, CV_32FC1 );
	CvMat* point_counts2			= cvCreateMat( n_boards, 1, CV_32SC1 );

	CvPoint2D32f* corners1 = new CvPoint2D32f[ board_n ];
	CvPoint2D32f* corners2 = new CvPoint2D32f[ board_n ];
	int corner_count1;
	int corner_count2;
	int successes = 0;
	int step, frame = 0;

	IplImage *image1 = cvQueryFrame( capture1 );
	IplImage *gray_image1 = cvCreateImage( cvGetSize( image1 ), 8, 1 );
	IplImage *image2 = cvQueryFrame( capture2 );
	IplImage *gray_image2 = cvCreateImage( cvGetSize( image2 ), 8, 1 );

	// Capture Corner views loop until we've got n_boards
	// succesful captures (all corners on the board are found)

	while( successes < n_boards ){
		// Skp every board_dt frames to allow user to move chessboard
		if( frame++ % board_dt == 0 ){
			// Find chessboard corners:
			int found1 = cvFindChessboardCorners( image1, board_sz, corners1,
				&corner_count1, CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FILTER_QUADS );
			int found2 = cvFindChessboardCorners( image2, board_sz, corners2,
				&corner_count2, CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FILTER_QUADS );

			// Get subpixel accuracy on those corners
			cvCvtColor( image1, gray_image1, CV_BGR2GRAY );
			cvCvtColor( image2, gray_image2, CV_BGR2GRAY );
			cvFindCornerSubPix( gray_image1, corners1, corner_count1, cvSize( 11, 11 ), 
				cvSize( -1, -1 ), cvTermCriteria( CV_TERMCRIT_EPS+CV_TERMCRIT_ITER, 30, 0.1 ));
			cvFindCornerSubPix( gray_image2, corners2, corner_count2, cvSize( 11, 11 ), 
				cvSize( -1, -1 ), cvTermCriteria( CV_TERMCRIT_EPS+CV_TERMCRIT_ITER, 30, 0.1 ));

			// Draw it
			cvDrawChessboardCorners( image1, board_sz, corners1, corner_count1, found1 );
			cvDrawChessboardCorners( image2, board_sz, corners2, corner_count2, found2 );
			cvShowImage( "Calibration1", image1 );
			cvShowImage( "Calibration2", image2 );

			// If we got a good board, add it to our data
			if( corner_count1 == board_n && corner_count2 == board_n ){
				step = successes*board_n;
				for( int i=step, j=0; j < board_n; ++i, ++j ){
					CV_MAT_ELEM( *image_points1, float, i, 0 ) = (float)(corners1[j].x);
					CV_MAT_ELEM( *image_points1, float, i, 1 ) = (float)(corners1[j].y);
					CV_MAT_ELEM( *object_points1, float, i, 0 ) = (float)(j/board_w);
					CV_MAT_ELEM( *object_points1, float, i, 1 ) = (float)(j%board_w);
					CV_MAT_ELEM( *object_points1, float, i, 2 ) = 0.0f;
					CV_MAT_ELEM( *image_points2, float, i, 0 ) = (float)(corners2[j].x);
					CV_MAT_ELEM( *image_points2, float, i, 1 ) = (float)(corners2[j].y);
					CV_MAT_ELEM( *object_points2, float, i, 0 ) = (float)(j/board_w);
					CV_MAT_ELEM( *object_points2, float, i, 1 ) = (float)(j%board_w);
					CV_MAT_ELEM( *object_points2, float, i, 2 ) = 0.0f;
				}
				CV_MAT_ELEM( *point_counts1, int, successes, 0 ) = board_n;
				CV_MAT_ELEM( *point_counts2, int, successes, 0 ) = board_n;
				successes++;
			}
		} 

		// Handle pause/unpause and ESC
		int c = cvWaitKey( 15 );
		if( c == 'p' ){
			c = 0;
			while( c != 'p' && c != 27 ){
				c = cvWaitKey( 250 );
			}
		}
		if( c == 27 )
			return 0;
		image1 = cvQueryFrame( capture1 ); // Get next image
		image2 = cvQueryFrame( capture2 ); // Get next image
	} // End collection while loop

	if (calibrateCameraPart2( MRotsA, MIntrinsicA, MTransA,
	board_n, successes, object_points1, image_points1,
	point_counts1, corners1, corner_count1, image1, gray_image1,
	capture1)!=0)
	{return -1;}
	if (calibrateCameraPart2( MRotsB, MIntrinsicB, MTransB,
	board_n, successes, object_points2, image_points2,
	point_counts2, corners2, corner_count2, image2, gray_image2,
	capture2)!=0)
	{return -1;}
	cvReleaseCapture( &capture1 );
	cvReleaseCapture( &capture2 );

	cvDestroyWindow("Calibration1");
	cvDestroyWindow("Calibration2");
	cvReleaseMat( &object_points1 );
	cvReleaseMat( &image_points1 );
	cvReleaseMat( &point_counts1 );
	cvReleaseMat( &object_points2 );
	cvReleaseMat( &image_points2 );
	cvReleaseMat( &point_counts2 );
	return 0;
}

int findMMatrices(
	vector<vector<double>>& M1,
	vector<vector<double>>& M2,
	vector<vector<double>>& Mext1,
	vector<vector<double>>& Mext2,
	vector<vector<double>>& MIntrinsic1,
	vector<vector<double>>& MIntrinsic2,
	int firstCamera = 0,
	int secondCamera = 1
	)
{
	M1.resize(3, vector<double>(4));
	M2.resize(3, vector<double>(4));
	vector<vector<vector<double>>> MRots1;
	vector<vector<double>> MTrans1;
	vector<vector<vector<double>>> MRots2;
	vector<vector<double>> MTrans2;


	LSRMath *lsr1;
	lsr1=new LSRMath(); 
	M1.resize(3, vector<double>(4));
	M2.resize(3, vector<double>(4));
	Mext1.resize(3, vector<double>(4));
	Mext2.resize(3, vector<double>(4));

	if (calibrateCamera( MRots1, MIntrinsic1, MTrans1, MRots2, MIntrinsic2, MTrans2, firstCamera, secondCamera )!=0) return -1;
	int successes= (int) MRots1.size();
	if (successes!=MRots2.size()) {return -1;}
	if (successes!=MTrans1.size()) {return -1;}
	if (successes!=MTrans2.size()) {return -1;}
	for (int i = 0; i<3; i++)
	{
		for (int j = 0; j<3; j++)
		{
			Mext1[i][j]=MRots1[successes-1][i][j];
			Mext2[i][j]=MRots2[successes-1][i][j];
		}
			Mext1[i][3]=MTrans1[successes-1][i];
			Mext2[i][3]=MTrans2[successes-1][i];
	}

	if (!lsr1->MultiplyMatrix(MIntrinsic1,Mext1,M1)==0) { return -1; }
	if (!lsr1->MultiplyMatrix(MIntrinsic2,Mext2,M2)==0) { return -1; }

	
	//This function is to test the accuracy of the stereo Reconstruction
	/*
	double pPairs[4]={201,170,187,199};
	vector<vector<double>> StereoPoint; // The projected stereo point.
	if (!lsr1->solveStereoReconstruction( M1, M2, pPairs, StereoPoint)==0) { return -1; }

	vector<vector<double>> imCoordsX1;
	vector<vector<double>> imCoordsX2;
	if (!lsr1->findImgCoordsFromWorld(StereoPoint, M1, imCoordsX1)==0) { return -1; }
	if (!lsr1->findImgCoordsFromWorld(StereoPoint, M2, imCoordsX2)==0) { return -1; }
	*/

	return 0;
}

int calculateBallPositions(
	char objectName[50],
	vector<vector<double>>& midPair1,
	vector<vector<double>>& midPair2,
	vector<double>& Tarr,
	int reconsCount,
	double midXobj,
	double midYobj,
	int firstCamera = 0,
	int secondCamera = 1,
	bool showPic = false
	)
{
	LSRMath *lsr1;
	lsr1=new LSRMath(); 
	
	midPair1.resize(reconsCount);
	midPair2.resize(reconsCount);

	char filename1[50];
	char filename2[50];
    string textdump1;

	Tarr.resize(reconsCount);

	for (int j = 0; j<reconsCount; j++)
	{

		takePicture(j,j,firstCamera);
		takePicture(j,j,secondCamera);

		SYSTEMTIME time;
		GetSystemTime(&time);
		double tempTarr = (double)((time.wMinute * 60 * 1000)+(time.wSecond * 1000) + time.wMilliseconds);

		sprintf(filename1, "Camera%dBall%d.jpg", firstCamera, j);
		sprintf(filename2, "Camera%dBall%d.jpg",  secondCamera, j);		
		
		vector<vector<vector<double>>> pointPairs1(reconsCount);
		vector<vector<vector<double>>> pointPairs2(reconsCount);
		while (!doSift(objectName,filename1,midPair1[j], pointPairs1[j], midXobj, midYobj, showPic)==0 ||
			!doSift(objectName,filename2,midPair2[j], pointPairs2[j], midXobj, midYobj, showPic)==0)
		{ 
			takePicture(j,j,firstCamera);
			takePicture(j,j,secondCamera);
		
			GetSystemTime(&time);
			tempTarr = (double)((time.wMinute * 60 * 1000)+(time.wSecond * 1000) + time.wMilliseconds);

			sprintf(filename1, "Camera%dBall%d.jpg", firstCamera, j);
			sprintf(filename2, "Camera%dBall%d.jpg",  secondCamera, j);		
		} 
		cout << "Logged a 3d point." << endl;
		Tarr[j] = tempTarr;
	}
return 0;
}

int calculateBallTrajectory(
	int argc, 
	char** argv, 
	vector<vector<double>> MExt1, 
	vector<vector<double>> MExt2,
	vector<vector<double>> MInt1,
	vector<vector<double>> MInt2,  
	vector<vector<double>>& solveVar,
	vector<vector<double>> M1,
	vector<vector<double>> M2,
	vector<vector<double>>& midPair1,
	vector<vector<double>>& midPair2,
	vector<double>& Tarr,
	char objectName[50],
	double midXobj,
	double midYobj,
	int firstCamera,
	int secondCamera,
	int reconsCount
	)
{



	LSRMath *lsr1;
	lsr1=new LSRMath(); 
	OpenGLDraw *ogld;
	ogld=new OpenGLDraw(); //this is our interface with OpenGL
	vector<vector<double>> XYZarr(3,vector<double>(0));
	vector<vector<double>> XYZarrTrans;

	SYSTEMTIME time;
	GetSystemTime(&time);
	double ttOriginal = (double)((time.wMinute * 60 * 1000) + (time.wSecond * 1000) + time.wMilliseconds);

		
	vector<vector<vector<double>>> Mcoords1(reconsCount);
	vector<vector<vector<double>>> Mcoords2(reconsCount);
	vector<vector<double>> AllpPairs(reconsCount,vector<double>(4));
	vector<double> TarrNow;

	for (int j = 0; j<reconsCount; j++)
	{
		//This scans for the target object and returns it's midpoint.
		if(calculateBallPositions( objectName, midPair1, midPair2, TarrNow,
		1, midXobj, midYobj, firstCamera, secondCamera, false )!=0){return -1;}
		Tarr.push_back((TarrNow[0] - ttOriginal)/1000.);
		//vector<double> pPairs;
		vector<vector<double>> coords(3, vector<double>(1)) ;
		AllpPairs[j][0]=(double) midPair1[0][2];
		AllpPairs[j][1]=(double) midPair1[0][3];
		AllpPairs[j][2]=(double) midPair2[0][2];
		AllpPairs[j][3]=(double) midPair2[0][3];


		if (!lsr1->solveStereoReconstruction( M1, M2, AllpPairs[j], coords )==0) { return -1; }
		XYZarr[0].push_back(coords[0][0]); //X input
		XYZarr[1].push_back(coords[1][0]); //Y input
		XYZarr[2].push_back(coords[2][0]); //Z input

		if (!lsr1->MMatrixTransform(coords, MExt1, Mcoords1[j])==0) { return -1; }
		if (!lsr1->MMatrixTransform(coords, MExt2, Mcoords2[j])==0) { return -1; }

		if (!lsr1->TransposeMatrix(XYZarr,XYZarrTrans)==0) { return -1; }
		if (j>4)
		{ 
			if (!lsr1->SolveParabolicXYZt(XYZarrTrans,Tarr,solveVar)==0) { return -1; }
			vector<vector<vector<double>>> targetPoint(j+1);
			for(int k = 0; k<j+1; k++)
				{ if (!lsr1->findParabolicPoint(solveVar,Tarr[k],targetPoint[k])==0) { return -1; } }
			if(ogld->DrawBallTrajectory( argc, argv, M1, M2, MExt1, MExt2, MInt1, MInt2, XYZarrTrans, Tarr, solveVar)!=0) { return -1; }
		}
	}
	return 0;
}

int OutputFuturePoints(vector<vector<double>> solveVar, vector<vector<double>> M1, vector<vector<double>> M2, IplImage *image1, IplImage *image2)
{
	LSRMath *lsr1;
	lsr1=new LSRMath(); 
	int Xsize = (int)solveVar.size();
	int Ysize = (int)solveVar[0].size();
	int FutureCoordCount = 21;
	double FutureCoordStep = 2;
	
	vector<vector<vector<double>> > FutureCoords(FutureCoordCount);
	vector<vector<vector<double>> > FutureImgCoords1(FutureCoordCount);
	vector<vector<vector<double>> > FutureImgCoords2(FutureCoordCount);

	for(int i = 0; i<FutureCoordCount; i++)
	{
		double timePoint = FutureCoordStep*(double)i;
		if (!lsr1->findParabolicPoint(solveVar,timePoint,FutureCoords[i])==0) { return -1; }
		if (!lsr1->findImgCoordsFromWorld(FutureCoords[i], M1, FutureImgCoords1[i])==0) { return -1; }
		if (!lsr1->findImgCoordsFromWorld(FutureCoords[i], M2, FutureImgCoords2[i])==0) { return -1; }

		cout << "The ball will be at {" << FutureCoords[i][0][0] << "," ;
		cout << FutureCoords[i][1][0] << "," ;
		cout << FutureCoords[i][2][0] << "} in ";
		cout << timePoint << " sec." << endl;

		cout << "    That is {" << FutureImgCoords1[i][0][0] << ", ";
		cout << FutureImgCoords1[i][1][0] << "}";
		cout << " in the first camera." << endl ;

		cout << "    That is {" << FutureImgCoords2[i][0][0] << ", ";
		cout << FutureImgCoords2[i][1][0] << "}";
		cout << " in the second camera." << endl ;
	}





	return 0;
}

int DrawVideoLine(IplImage *image, int x1, int x2, int y1, int y2)
{
	  static CvScalar colors[] = 
    {
        {{0,0,255}},
        {{0,128,255}},
        {{0,255,255}},
        {{0,255,0}},
        {{255,128,0}},
        {{255,255,0}},
        {{255,0,0}},
        {{255,0,255}},
        {{255,255,255}}
    };
	cvLine( image, cvPoint(x1, y1 ), cvPoint(x2, y2 ), colors[8] );
	cvLine( image, cvPoint(x1+1, y1+1 ), cvPoint(x2, y2 ), colors[7] );
	return 0;
}

int startVideos(IplImage *image1, IplImage *image2)
{
	cvNamedWindow( "Video1" );
	cvNamedWindow( "Video2" );

	cvShowImage( "Video1", image1 );
	cvShowImage( "Video2", image2 );

	
	int c = cvWaitKey( 15 );
	if( c == 'p' ){
		c = 0;
		while( c != 'p' && c != 27 ){
			c = cvWaitKey( 250 );
		}
	}
	if( c == 27 )
	{
		cvDestroyWindow("Video1");
		cvDestroyWindow("Video2");
	}
	
	return 0;
}


int CalcAxis(
	vector<vector<double>> v1,
	vector<vector<double>> v2,
	vector<vector<double>> c1,
	vector<vector<double>> c2,
	vector<vector<double>>& v3,
	vector<vector<double>>& c3,
	double& angle1,
	double& angle2
	)
{
	vector<vector<double>> LineParams;
	LSRMath *lsr1;
	lsr1=new LSRMath(); 
	vector<vector<double>> Amatrix(3,vector<double>(2));
	vector<vector<double>> Bmatrix(3,vector<double>(1));
	for( int i = 0; i<3; i++)
	{
		Amatrix[i][0]=v1[i][0];
		Amatrix[i][1]=-1*v2[i][0];
		Bmatrix[i][0]=c1[i][0]-c2[i][0];
	}
		if (!lsr1->SolveLSR(Amatrix,Bmatrix,LineParams,"")==0) { return -1; }
		
	c3.resize(3,vector<double>(1));
	for( int i = 0; i<3; i++)
	{
		c3[i][0]=c1[i][0]+c2[i][0]+(LineParams[0][0]*v1[i][0]+LineParams[1][0]*v2[i][0])/2;
	}
		double anglecos;
		double anglesin;
		if (!lsr1->CrossProduct(v1,v2,v3)==0) { return -1; }
		if (!lsr1->DotProduct(v1,v2,anglecos)==0) { return -1; }
		if (!lsr1->DotProduct(v3,v3,anglesin)==0) { return -1; }
		angle1 = asin(anglesin)*180/M_PI;
		angle2 = acos(anglecos)*180/M_PI;
		return 0;
}

int LaserCalcAxis(
		vector<vector<vector<double>>> LaserCoords1,
		vector<vector<vector<double>>> LaserCoords2,
		vector<vector<double>>& v3,
		vector<vector<double>>& c3,
		double& angle1,
		double& angle2
		)
{
	LSRMath *lsr1;
	lsr1=new LSRMath(); 
	vector<vector<double>> v1(3,vector<double>(1));
	vector<vector<double>> v2(3,vector<double>(1));
	vector<vector<double>> c1(3,vector<double>(1));
	vector<vector<double>> c2(3,vector<double>(1));

	
	for( int i = 0; i<3; i++)
	{
		v1[i][0]=LaserCoords1[1][i][0]-LaserCoords1[0][i][0];
		v2[i][0]=LaserCoords2[1][i][0]-LaserCoords2[0][i][0];
		c1[i][0]=LaserCoords1[0][i][0];
		c2[i][0]=LaserCoords2[0][i][0];
	} 
	if (!lsr1->Normalize(v1)==0) { return -1; }
	if (!lsr1->Normalize(v2)==0) { return -1; }
	if( CalcAxis( v1, v2, c1, c2, v3, c3, angle1, angle2 )!=0 ){ return -1; }
	if (!lsr1->Normalize(v3)==0) { return -1; }
	return 0;
}

int calculateLaserPoints(
	vector<vector<double>> M1,
	vector<vector<double>> M2,
	int firstCamera,
	int secondCamera,
	vector<vector<vector<double>>>& LaserCoords
	)
{
	double TargetMidX = 76.;
	double TargetMidY = 57.;
	LSRMath *lsr1;
	lsr1=new LSRMath(); 
	vector<vector<vector<vector<double>>>> pointPairs1(2);
	vector<vector<vector<vector<double>>>> pointPairs2(2);
	vector<double> Tarr;

	vector<vector<vector<double>>> midPair1(2);
	vector<vector<vector<double>>> midPair2(2);

    string textdump1;
	textdump1="";
	cout << "Press a value then enter to scan for the first laser point.";
	cin >> textdump1;
	textdump1="r";

	while (textdump1=="r")
	{
		if(calculateBallPositions( "BaldEagle.jpg", midPair1[0], midPair2[0], Tarr,
			1, TargetMidX, TargetMidY, firstCamera, secondCamera )!=0){ return -1; }
		cout << "Type 'r' to redo. Type 'k' to continue.";
		cin >> textdump1;
	}
	cout << "Press a value then enter to scan for the second laser point.";
	cin >> textdump1;
	textdump1="r";

	
	while (textdump1=="r")
	{
		if(calculateBallPositions( "BaldEagle.jpg", midPair1[1], midPair2[1], Tarr,
			1, TargetMidX, TargetMidY, firstCamera, secondCamera )!=0){ return -1; }
		cout << "Type 'r' to redo. Type 'k' to continue.";
		cin >> textdump1;
	}
	LaserCoords.resize(2,vector<vector<double>>(3, vector<double>(1))) ;
	for (int j = 0; j<2; j++)
	{
//		double ppAvg1[4];
//		double ppAvg2[4];
		vector<double> pPairs(4);
		pPairs[0]=(double) midPair1[j][0][2];
		pPairs[1]=(double) midPair1[j][0][3];
		pPairs[2]=(double) midPair2[j][0][2];
		pPairs[3]=(double) midPair2[j][0][3];
		if (!lsr1->solveStereoReconstruction( M1, M2, pPairs, LaserCoords[j])==0) { return -1; }
		vector<vector<double>> imCoords1;
		vector<vector<double>> imCoords2;
		//if (!lsr1->findImgCoordsFromWorld(LaserCoords[j], M1, imCoords1)) { return -1; }
		//if (!lsr1->findImgCoordsFromWorld(LaserCoords[j], M2, imCoords2)) { return -1; }

	}

	return 0;
}

int calculateRotationalAngles(
							  vector<vector<double>> targetPoint,
							  vector<vector<double>> Mext1,
							  vector<vector<double>> Mext2,
							  double& RotationAngle,
							  double& BowAngle
							  )
{
	LSRMath *lsr1;
	lsr1=new LSRMath(); 
	vector<vector<double>> tp1;
	vector<vector<double>> tp2;
	vector<vector<double>> tpMid(3,vector<double>(1));
	if (lsr1->MMatrixTransform( targetPoint, Mext1, tp1)!=0) {return -1;}
	if (lsr1->MMatrixTransform( targetPoint, Mext2, tp2)!=0) {return -1;}
	for (int i = 0; i<3; i++)
	{
		tpMid[i][0]=tp1[i][0]+tp2[i][0];
	}
	double RotTan = tpMid[0][0]/tpMid[2][0];
	double BowTan = tpMid[1][0]/tpMid[2][0];
	RotationAngle = atan(RotTan)*180/M_PI;
	BowAngle=atan(BowTan)*180/M_PI-90;
	
	return 0;
}


int calculateRotationalAngles2(
							  vector<vector<vector<double>>> LaserCoords,
							  vector<vector<double>> targetPoint,
							  vector<vector<double>> MRobot,
							  double& RotationAngle,
							  double& BowAngle )
{
	LSRMath *lsr1;
	lsr1=new LSRMath(); 
	vector<vector<double>> lp1;
	vector<vector<double>> lp2;
	vector<vector<double>> tp;
	vector<vector<double>> laserVect(3,vector<double>(1));
	if (lsr1->MMatrixTransform( LaserCoords[0], MRobot, lp1)!=0) {return -1;}
	if (lsr1->MMatrixTransform( LaserCoords[1], MRobot, lp2)!=0) {return -1;}
	if (lsr1->MMatrixTransform( targetPoint, MRobot, tp)!=0) {return -1;}
	vector<double> l1;
	vector<double> l2;
	if(lsr1->FindLengths( lp1, l1 )!=0){return -1;}
	if(lsr1->FindLengths( lp2, l2 )!=0){return -1;}
	
	// Choose the farther point on the laser for more accurate results.
	if(l1[0]<l2[0])
		{ for(int i = 0; i < 3; i++){laserVect[i][0]=lp2[i][0];} }
	else
		{ for(int i = 0; i < 3; i++){laserVect[i][0]=lp1[i][0];} }

	laserVect[1][0]=0; // The last laser vect is only good for finding my RotationAngle, and is useless for BowAngle.

	if(lsr1->Normalize( laserVect )!=0){return -1;}	//This will give the normalized vector from the center to a point on the laser.
	if(lsr1->Normalize( tp )!=0){return -1;} //This will give the normalized vector from the center to the point.
	double cosAngle;
	if(lsr1->DotProduct( laserVect, tp, cosAngle)!=0){return -1;}
	RotationAngle = acos(cosAngle)*180/M_PI;
	BowAngle=acos(tp[1][0])*180/M_PI-90;
	return 0;
}
int main(int argc, char** argv)
{

	LSRMath *lsr1;
	lsr1=new LSRMath(); 
	nxt_remote *nxtr;
	nxtr=new nxt_remote(); //this is our interface with the NXT
	char* comPort = "COM20";


	OpenGLDraw *ogld;
	ogld=new OpenGLDraw(); //this is our interface with OpenGL
	//ogld->OpenGLTest( argc, argv );
    string textdump1;
	int firstCamera = 0;
	int secondCamera = 1;

	//takePicture(10, 19, firstCamera); //This is test code we wrote to take many pictures.
	//takePicture(10, 19, secondCamera); //This is test code we wrote to take many pictures.
	CvCapture* capture1 = cvCreateCameraCapture( firstCamera ); // This initializes and tests the cameras for capturing
	CvCapture* capture2 = cvCreateCameraCapture( secondCamera ); // This initializes and tests the cameras for capturing
	assert( capture1 );
	assert( capture2 );
/*	
	IplImage *image1 = cvQueryFrame( capture1 );
	IplImage *image2 = cvQueryFrame( capture2 );
	startVideos(image1, image2);
	DrawVideoLine(image1, 0, 200, 0, 200);
	DrawVideoLine(image2, 0, 200, 0, 200);
	DrawVideoLine(image1, 200, 0, 0, 200);
	DrawVideoLine(image2, 200, 0, 0, 200);
*/
	
	//if (stopNxt(nxtr)!=0) { return -1; } //Uncomment this to stop annoying sound... and the program.
	//if(initNxt(nxtr,comPort)!=0){ return -1;} //This initializes the robot's bluetooth connection.
	//if (stopNxt(nxtr)!=0) { return -1; }  //Uncomment this to stop annoying sound... and the program.
	
	vector<vector<double>> M1; // The M matrix for camera 1.
	vector<vector<double>> M2; // The M matrix for camera 2.
	vector<vector<double>> MExt1;
	vector<vector<double>> MExt2;
	vector<vector<double>> MInt1;
	vector<vector<double>> MInt2;
	vector<vector<double>> midPair1; // The midpoint to midpoint pairs for camera 1.
	vector<vector<double>> midPair2; // The midpoint to midpoint pairs for camera 2.
	vector<double> Tarr; //The Time dimension for the trajectory
	vector<vector<double>> solveVar; //The solved trajectory.
	int reconsCount = 15; //The number of pictures we take before estimating trajectory (3+ is required)
	vector<vector<vector<vector<double>>>> LaserCoords(3); // The laser point coordinates
	double bullseyeX = 76.; //The real center of the target image
	double bullseyeY = 57.; //The real center of the target image


	
	//if(calculateBallPositions( "BaldEagle.jpg", midPair1, midPair2, Tarr,
	//	6, bullseyeX, bullseyeY, firstCamera, secondCamera )!=0){return -1;}
	if(findMMatrices( M1, M2, MExt1, MExt2, MInt1, MInt2, firstCamera, secondCamera)!=0) { return -1; } //This function calibrates the cameras
	
	vector<double> RotXYZ1;
	vector<double> RotXYZ2;
	if(lsr1->FindRotationAngles( MExt1, RotXYZ1 )!=0){return -1;}
	if(lsr1->FindRotationAngles( MExt2, RotXYZ2 )!=0){return -1;}

	if(ogld->DrawAxes( argc, argv, M1, M2)!=0) { return -1; }
	vector<vector<double>> vLaserY;
	vector<vector<double>> cLaserY;
	vector<vector<double>> vLaserX;
	vector<vector<double>> cLaserX;
	double angleY1;
	double angleX1;	
	double angleY2;
	double angleX2;

	/*
		//if(runRobot(0,10,false,nxtr)!=0){ return -1; }
		cout << "Calculating original position." << endl;
		if(calculateLaserPoints( M1, M2, firstCamera, secondCamera, LaserCoords[0] )!=0){ return -1; }
		//if(runRobot(15,0,false,nxtr)!=0){ return -1; }
		cout << "Calculating secondary position." << endl;
		if(calculateLaserPoints( M1, M2, firstCamera, secondCamera, LaserCoords[1] )!=0){ return -1; }
		//if(runRobot(0,0,true,nxtr)!=0){ return -1; }
		//if(runRobot(0,10,false,nxtr)!=0){ return -1; }
		cout << "Calculating tertiary position." << endl;
		if(calculateLaserPoints( M1, M2, firstCamera, secondCamera, LaserCoords[2] )!=0){ return -1; }
		//if(runRobot(0,10,true,nxtr)!=0){ return -1; }
		
		if(ogld->DrawLasers( argc, argv, M1, M2, LaserCoords)!=0) { return -1; }

		//To find the axis, we take the cross product of the two laser lines, and start it at the estimated intersection.
		//The angles are returned for testing purposes
		if (!LaserCalcAxis( LaserCoords[0], LaserCoords[1], vLaserY, cLaserY, angleY1, angleY2)==0) { return -1; }
		if (!LaserCalcAxis( LaserCoords[1], LaserCoords[2], vLaserX, cLaserX, angleX1, angleX2)==0) { return -1; }

		//Find the M Matrix for the Robot and project it

		vector<vector<double>> MRobot;
		
		if(ogld->DrawLaserAxes( argc, argv, MExt1, MExt2, MInt1, MInt2, vLaserX, vLaserY )!=0) { return -1; }
		if (lsr1->findMMatrixFromLaserAxes( vLaserX, cLaserX, vLaserY, cLaserY, MRobot )!=0){return -1;}
	*/

	

	cout << "Press a value then enter to scan for the target.";
	cin >> textdump1;

	//This scans for the target object and returns it's midpoint.
	//if(calculateBallPositions( "BaldEagle.jpg", midPair1, midPair2, Tarr,
	//	reconsCount, bullseyeX, bullseyeY, firstCamera, secondCamera )!=0){return -1;}
	
	//This performs stereo reconstruction to find the exact location of the target object.
	//Then it uses the set of points to estimate the parabolic curve.
	if(calculateBallTrajectory( argc, argv, MExt1, MExt2, MInt1, MInt2, solveVar, M1, M2, midPair1, midPair2,
		Tarr, "BaldEagle.jpg", bullseyeX, bullseyeY, firstCamera, secondCamera, reconsCount)!=0){return -1;}
	//if(ogld->DrawBallTrajectory( argc, argv, M1, M2, MExt1, MExt2, MInt1, MInt2,Tarr, solveVar)!=0) { return -1; }
	vector<vector<double>> targetPoint;
	double RotationAngle;
	double BowAngle;
	int TSize=(int) Tarr.size();
	double timePoint=Tarr[TSize-1]+4.; // This targets 4 seconds after the last time

	if (!lsr1->findParabolicPoint(solveVar,timePoint,targetPoint)==0) { return -1; }
	if (!calculateRotationalAngles( targetPoint, MExt1, MExt2,  RotationAngle, BowAngle )==0){return -1;}
	if(runRobot(RotationAngle,BowAngle,false,nxtr)!=0){ return -1; }

//	if(OutputFuturePoints(solveVar, M1,  M2, image1, image2)!=0){return -1;}


	if (stopNxt(nxtr)!=0) { return -1; }
	return 0;

}
