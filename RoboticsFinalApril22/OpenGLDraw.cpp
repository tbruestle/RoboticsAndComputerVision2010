#include <vector>
#include <iterator>
#include "OpenGLDraw.h"
#include "LSRMath.h"
#include <math.h>
#include <stdlib.h> 
#include <stdio.h> 
#include <cv.h>
#include <highgui.h>
#include <string.h>
#include "openglut.h"
#include "openglut_std.h"
#include "openglut_ext.h"
#include "openglut_exp.h"

//#include <GL/glut.h>

using namespace std;


vector<vector<vector<vector<double>>>> ImCoords1;
vector<vector<vector<vector<double>>>> ImCoords2;
vector<vector<vector<vector<double>>>> ImCoordsX1;
vector<vector<vector<vector<double>>>> ImCoordsX2;
vector<vector<vector<double>>> ImCoords3d;


#define VIEWPORT_WIDTH              320
#define VIEWPORT_HEIGHT             240
#define KEY_ESCAPE                  27


CvCapture* g_Capture1;
CvCapture* g_Capture2;
GLint g_hWindow1;

vector<double> RotXYZ1(3);
vector<double> RotXYZ2(3);
vector<double> TransXYZ1(3);
vector<double> TransXYZ2(3);

GLvoid InitAxesGL();
GLvoid InitLaserAxesGL();
GLvoid OnDisplayLaserAxes(void);
GLvoid OnDisplayAxes();
GLvoid OnReshape(GLint w, GLint h);
GLvoid OnKeyPress (unsigned char key, GLint x, GLint y);
static void argConvGLcpara2( double cparam[3][4], int width, int height, double gnear, double gfar, double m[16] );
GLvoid OnIdle();
GLvoid OnDisplayBallTrajectory(void);


GLvoid InitAxesGL()
{  

	glClearColor (0.0, 0.0, 0.0, 0.0);
	glDisable(GL_BLEND);
	glutDisplayFunc(OnDisplayAxes);
	glutReshapeFunc(OnReshape);
	glutKeyboardFunc(OnKeyPress);
	glutIdleFunc(OnIdle);

}


GLvoid InitLaserAxesGL()
{  

	glClearColor (0.0, 0.0, 0.0, 0.0);
	glDisable(GL_BLEND);
	glutDisplayFunc(OnDisplayLaserAxes);
	glutReshapeFunc(OnReshape);
	glutKeyboardFunc(OnKeyPress);
	glutIdleFunc(OnIdle);

}



GLvoid InitBallTrajectoryGL()
{  

	glClearColor (0.0, 0.0, 0.0, 0.0);
	glDisable(GL_BLEND);
	glutDisplayFunc(OnDisplayBallTrajectory);
	glutReshapeFunc(OnReshape);
	glutKeyboardFunc(OnKeyPress);
	glutIdleFunc(OnIdle);

}

GLvoid OnDisplayLaserAxes(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_TEXTURE_2D);

	// Set Projection Matrix
	glMatrixMode (GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, VIEWPORT_WIDTH*2, VIEWPORT_HEIGHT, 0);

	// Switch to Model View Matrix
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glBindTexture(GL_TEXTURE_2D, 1);
	
	// Draw a textured quad
	glBegin(GL_QUADS);
		glTexCoord2f(0.0f, 0.0f); glVertex2f(0.0f, 0.0f);
		glTexCoord2f(1.0f, 0.0f); glVertex2f(VIEWPORT_WIDTH, 0.0f);
		glTexCoord2f(1.0f, 1.0f); glVertex2f(VIEWPORT_WIDTH, VIEWPORT_HEIGHT);
		glTexCoord2f(0.0f, 1.0f); glVertex2f(0.0f, VIEWPORT_HEIGHT);
	glEnd();

	
	glBindTexture(GL_TEXTURE_2D, 2);

	glBegin(GL_QUADS);
		glTexCoord2f(0.0f, 0.0f); glVertex2f(VIEWPORT_WIDTH, 0.0f);
		glTexCoord2f(1.0f, 0.0f); glVertex2f(VIEWPORT_WIDTH*2.0f, 0.0f);
		glTexCoord2f(1.0f, 1.0f); glVertex2f(VIEWPORT_WIDTH*2.0f, VIEWPORT_HEIGHT);
		glTexCoord2f(0.0f, 1.0f); glVertex2f(VIEWPORT_WIDTH, VIEWPORT_HEIGHT);
	glEnd();


	/*
		glPushMatrix();
		glScaled(1,1,-1);
		glRotated(35,0,1,0);
		glTranslated(1,1,1);
		glPopMatrix();
	*/
	glColor3f(0.0,1.0,0.0);
	glLineWidth(5.0);
	int CoordCount = (int) ImCoords1.size();
	if (CoordCount==3)
	{
				glBegin( GL_LINES );
				glLineWidth(5.0);

				
				
			for (int i = 0; i < 3; i++)
			{      
				glColor3f(1.0,1.0,1.0);
				glVertex2f( (GLfloat) ImCoords1[i][0][0][0]+1.0f , (GLfloat) ImCoords1[i][0][1][0] +1.0f );
				glVertex2f( (GLfloat) ImCoords1[i][1][0][0]+1.0f , (GLfloat) ImCoords1[i][1][1][0] +1.0f );
				glVertex2f( (GLfloat) ImCoords2[i][0][0][0] + 320.0f +1.0f , (GLfloat) ImCoords2[i][0][1][0]+1.0f  );
				glVertex2f( (GLfloat) ImCoords2[i][1][0][0] + 320.0f +1.0f , (GLfloat) ImCoords2[i][1][1][0]+1.0f  );

				switch ( i )
				{
					case 0:
						glColor3f(1.0,0.0,0.0);
						break;
					case 1:
						glColor3f(0.0,1.0,0.0);
						break;
					case 2:
						glColor3f(0.0,0.0,1.0);
						break;
					default:
						glColor3f(0.0,0.0,0.0);
				}

				glVertex2f( (GLfloat) ImCoords1[i][0][0][0], (GLfloat) ImCoords1[i][0][1][0] );
				glVertex2f( (GLfloat) ImCoords1[i][1][0][0], (GLfloat) ImCoords1[i][1][1][0] );
				glVertex2f( (GLfloat) ImCoords2[i][0][0][0] + 320.0f , (GLfloat) ImCoords2[i][0][1][0] );
				glVertex2f( (GLfloat) ImCoords2[i][1][0][0] + 320.0f , (GLfloat) ImCoords2[i][1][1][0] );
			}

	}
	else
	{
		glBegin( GL_LINES );
			for (int i = 0; i < CoordCount; i++)
			{
			glVertex2f( (GLfloat) ImCoords1[i][0][0][0]+1.0f , (GLfloat) ImCoords1[i][0][1][0] +1.0f );
			glVertex2f( (GLfloat) ImCoords1[i][1][0][0]+1.0f , (GLfloat) ImCoords1[i][1][1][0] +1.0f );
			}

			for (int i = 0; i < CoordCount; i++)
			{
			glVertex2f( (GLfloat) ImCoords2[i][0][0][0] + 320.0f +1.0f , (GLfloat) ImCoords2[i][0][1][0]+1.0f  );
			glVertex2f( (GLfloat) ImCoords2[i][1][0][0] + 320.0f +1.0f , (GLfloat) ImCoords2[i][1][1][0]+1.0f  );
			}

		glColor3f(1.0,0.0,0.0);
		glLineWidth(5.0);
		glBegin( GL_LINES );
			for (int i = 0; i < CoordCount; i++)
			{
			glVertex2f( (GLfloat) ImCoords1[i][0][0][0], (GLfloat) ImCoords1[i][0][1][0] );
			glVertex2f( (GLfloat) ImCoords1[i][1][0][0], (GLfloat) ImCoords1[i][1][1][0] );
			}

			for (int i = 0; i < CoordCount; i++)
			{
			glVertex2f( (GLfloat) ImCoords2[i][0][0][0] + 320.0f , (GLfloat) ImCoords2[i][0][1][0] );
			glVertex2f( (GLfloat) ImCoords2[i][1][0][0] + 320.0f , (GLfloat) ImCoords2[i][1][1][0] );
			

			}
	}
		//glEnable(GL_NORMALIZE);
		//gluSphere();
		//glutSolidSphere(1,32,16);

	glEnd();
	glColor3f(1.0,1.0,1.0);



	glFlush();
	glutSwapBuffers();

}



GLvoid OnDisplayAxes(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_TEXTURE_2D);

	// Set Projection Matrix
	glMatrixMode (GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, VIEWPORT_WIDTH*2, VIEWPORT_HEIGHT, 0);

	// Switch to Model View Matrix
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glBindTexture(GL_TEXTURE_2D, 1);
	
	// Draw a textured quad
	glBegin(GL_QUADS);
		glTexCoord2f(0.0f, 0.0f); glVertex2f(0.0f, 0.0f);
		glTexCoord2f(1.0f, 0.0f); glVertex2f(VIEWPORT_WIDTH, 0.0f);
		glTexCoord2f(1.0f, 1.0f); glVertex2f(VIEWPORT_WIDTH, VIEWPORT_HEIGHT);
		glTexCoord2f(0.0f, 1.0f); glVertex2f(0.0f, VIEWPORT_HEIGHT);
	glEnd();

	
	glBindTexture(GL_TEXTURE_2D, 2);

	glBegin(GL_QUADS);
		glTexCoord2f(0.0f, 0.0f); glVertex2f(VIEWPORT_WIDTH, 0.0f);
		glTexCoord2f(1.0f, 0.0f); glVertex2f(VIEWPORT_WIDTH*2.0f, 0.0f);
		glTexCoord2f(1.0f, 1.0f); glVertex2f(VIEWPORT_WIDTH*2.0f, VIEWPORT_HEIGHT);
		glTexCoord2f(0.0f, 1.0f); glVertex2f(VIEWPORT_WIDTH, VIEWPORT_HEIGHT);
	glEnd();



	glColor3f(0.0,1.0,0.0);
	glLineWidth(5.0);
	int CoordCount = (int) ImCoords1.size();
	if (CoordCount==3)
	{
				glBegin( GL_LINES );
				glLineWidth(5.0);

				
				
			for (int i = 0; i < 3; i++)
			{      
				glColor3f(1.0,1.0,1.0);
				glVertex2f( (GLfloat) ImCoords1[i][0][0][0]+1.0f , (GLfloat) ImCoords1[i][0][1][0] +1.0f );
				glVertex2f( (GLfloat) ImCoords1[i][1][0][0]+1.0f , (GLfloat) ImCoords1[i][1][1][0] +1.0f );
				glVertex2f( (GLfloat) ImCoords2[i][0][0][0] + 320.0f +1.0f , (GLfloat) ImCoords2[i][0][1][0]+1.0f  );
				glVertex2f( (GLfloat) ImCoords2[i][1][0][0] + 320.0f +1.0f , (GLfloat) ImCoords2[i][1][1][0]+1.0f  );

				switch ( i )
				{
					case 0:
						glColor3f(1.0,0.0,0.0);
						break;
					case 1:
						glColor3f(0.0,1.0,0.0);
						break;
					case 2:
						glColor3f(0.0,0.0,1.0);
						break;
					default:
						glColor3f(0.0,0.0,0.0);
				}

				glVertex2f( (GLfloat) ImCoords1[i][0][0][0], (GLfloat) ImCoords1[i][0][1][0] );
				glVertex2f( (GLfloat) ImCoords1[i][1][0][0], (GLfloat) ImCoords1[i][1][1][0] );
				glVertex2f( (GLfloat) ImCoords2[i][0][0][0] + 320.0f , (GLfloat) ImCoords2[i][0][1][0] );
				glVertex2f( (GLfloat) ImCoords2[i][1][0][0] + 320.0f , (GLfloat) ImCoords2[i][1][1][0] );
			}

	}
	else
	{
		glBegin( GL_LINES );
			for (int i = 0; i < CoordCount; i++)
			{
			glVertex2f( (GLfloat) ImCoords1[i][0][0][0]+1.0f , (GLfloat) ImCoords1[i][0][1][0] +1.0f );
			glVertex2f( (GLfloat) ImCoords1[i][1][0][0]+1.0f , (GLfloat) ImCoords1[i][1][1][0] +1.0f );
			}

			for (int i = 0; i < CoordCount; i++)
			{
			glVertex2f( (GLfloat) ImCoords2[i][0][0][0] + 320.0f +1.0f , (GLfloat) ImCoords2[i][0][1][0]+1.0f  );
			glVertex2f( (GLfloat) ImCoords2[i][1][0][0] + 320.0f +1.0f , (GLfloat) ImCoords2[i][1][1][0]+1.0f  );
			}

		glColor3f(1.0,0.0,0.0);
		glLineWidth(5.0);
		glBegin( GL_LINES );
			for (int i = 0; i < CoordCount; i++)
			{
			glVertex2f( (GLfloat) ImCoords1[i][0][0][0], (GLfloat) ImCoords1[i][0][1][0] );
			glVertex2f( (GLfloat) ImCoords1[i][1][0][0], (GLfloat) ImCoords1[i][1][1][0] );
			}

			for (int i = 0; i < CoordCount; i++)
			{
			glVertex2f( (GLfloat) ImCoords2[i][0][0][0] + 320.0f , (GLfloat) ImCoords2[i][0][1][0] );
			glVertex2f( (GLfloat) ImCoords2[i][1][0][0] + 320.0f , (GLfloat) ImCoords2[i][1][1][0] );
			

			}
	}
		//glEnable(GL_NORMALIZE);
		//gluSphere();
		//glutSolidSphere(1,32,16);

	glEnd();
	glColor3f(1.0,1.0,1.0);



	glFlush();
	glutSwapBuffers();

}


GLvoid OnDisplayBallTrajectory(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_TEXTURE_2D);

	// Set Projection Matrix
	glMatrixMode (GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, VIEWPORT_WIDTH*2, VIEWPORT_HEIGHT, 0);

	// Switch to Model View Matrix
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glBindTexture(GL_TEXTURE_2D, 1);
	
	// Draw a textured quad
	glBegin(GL_QUADS);
		glTexCoord2f(0.0f, 0.0f); glVertex2f(0.0f, 0.0f);
		glTexCoord2f(1.0f, 0.0f); glVertex2f(VIEWPORT_WIDTH, 0.0f);
		glTexCoord2f(1.0f, 1.0f); glVertex2f(VIEWPORT_WIDTH, VIEWPORT_HEIGHT);
		glTexCoord2f(0.0f, 1.0f); glVertex2f(0.0f, VIEWPORT_HEIGHT);
	glEnd();

	
	glBindTexture(GL_TEXTURE_2D, 2);

	glBegin(GL_QUADS);
		glTexCoord2f(0.0f, 0.0f); glVertex2f(VIEWPORT_WIDTH, 0.0f);
		glTexCoord2f(1.0f, 0.0f); glVertex2f(VIEWPORT_WIDTH*2.0f, 0.0f);
		glTexCoord2f(1.0f, 1.0f); glVertex2f(VIEWPORT_WIDTH*2.0f, VIEWPORT_HEIGHT);
		glTexCoord2f(0.0f, 1.0f); glVertex2f(VIEWPORT_WIDTH, VIEWPORT_HEIGHT);
	glEnd();

	

	int CoordCount = (int) ImCoords1.size();
	int CoordCount2 = (int) ImCoordsX1.size();

	glBegin( GL_LINES );
	glLineWidth(5.0);
		
	for (int i = 0; i < CoordCount; i++)
	{      
		switch ( i%2 )
		{
			case 0:
				glColor3f(1.0,0.0,0.0);
				break;
			case 1:
				glColor3f(0.0,1.0,0.0);
				break;
			default:
				glColor3f(0.0,0.0,0.0);
		}
		glVertex2f( (GLfloat) ImCoords1[i][0][0][0], (GLfloat) ImCoords1[i][0][1][0] );
		glVertex2f( (GLfloat) ImCoords1[i][1][0][0], (GLfloat) ImCoords1[i][1][1][0] );
		glVertex2f( (GLfloat) ImCoords2[i][0][0][0] + 320.0f , (GLfloat) ImCoords2[i][0][1][0] );
		glVertex2f( (GLfloat) ImCoords2[i][1][0][0] + 320.0f , (GLfloat) ImCoords2[i][1][1][0] );
	}
	
	glColor3f(0.0,0.0,1.0);
	for (int i = 0; i < CoordCount2; i++)
	{ 
		glVertex2f( (GLfloat) ImCoordsX1[i][0][0][0], (GLfloat) ImCoordsX1[i][0][1][0] );
		glVertex2f( (GLfloat) ImCoordsX1[i][1][0][0], (GLfloat) ImCoordsX1[i][1][1][0] );
		glVertex2f( (GLfloat) ImCoordsX2[i][0][0][0] + 320.0f , (GLfloat) ImCoordsX2[i][0][1][0] );
		glVertex2f( (GLfloat) ImCoordsX2[i][1][0][0] + 320.0f , (GLfloat) ImCoordsX2[i][1][1][0] ); 
	}


		glEnd();
	int CoordCount3d = (int) ImCoords3d.size();
	/*
	for (int i = 0; i < CoordCount3d; i+=3)
	{  
			glPushMatrix();
			glScaled(1,1,1);
			//glRotated(35,0,1,0);
			glTranslated(
							0*-1*ImCoords3d[i][0][0],
							0*-1*ImCoords3d[i][1][0],
							0*-1*ImCoords3d[i][2][0]
						);
			glPopMatrix();
			
			glEnable(GL_NORMALIZE);
			//gluSphere();
			glutWireSphere(10,32,16);

			
			glPushMatrix();
			glScaled(1,1,1);
			//glRotated(35,0,1,0);
			glTranslated(
							0*1*ImCoords3d[i][0][0],
							0*1*ImCoords3d[i][1][0],
							0*1*ImCoords3d[i][2][0]
						);
			glPopMatrix();
			
	}
	*/
		glColor3f(1.0,1.0,1.0);

		glFlush();
		glutSwapBuffers();
	}

GLvoid OnReshape(GLint w, GLint h)
{
	glViewport(0, 0, w, h);
}

GLvoid OnKeyPress(unsigned char key, int x, int y)
{
	//if (key == KEY_ESCAPE)
	//{
	cvReleaseCapture(&g_Capture1);
	cvReleaseCapture(&g_Capture2);
	glutDestroyWindow(g_hWindow1);
	glutLeaveMainLoop();
	//}
}


GLvoid OnIdle()
{
		// Capture next frame
		IplImage *image1 = cvQueryFrame(g_Capture1);
		IplImage *image2 = cvQueryFrame(g_Capture2);

		// Convert to RGB
		cvCvtColor(image1, image1, CV_BGR2RGB);
		cvCvtColor(image2, image2, CV_BGR2RGB);
		
		// Create Texture
		glBindTexture(GL_TEXTURE_2D, 1);
		gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB, image1->width, image1->height, GL_RGB, GL_UNSIGNED_BYTE, image1->imageData);

		glBindTexture(GL_TEXTURE_2D, 2);
		gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB, image2->width, image2->height, GL_RGB, GL_UNSIGNED_BYTE, image2->imageData);

		// Update View port
		glutPostRedisplay();
}

int OpenGLDraw::DrawLasers( int argc, char** argv, vector<vector<double>> M1, vector<vector<double>> M2, vector<vector<vector<vector<double>>>> LaserCoords)
{

	LSRMath *lsr1;
	lsr1=new LSRMath(); 
	if(lsr1->findImageAxesPoints( LaserCoords, M1, ImCoords1)!=0){return -1;}
	if(lsr1->findImageAxesPoints( LaserCoords, M2, ImCoords2)!=0){return -1;}

	// Create GLUT Window
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(VIEWPORT_WIDTH*2, VIEWPORT_HEIGHT);

	g_hWindow1 = glutCreateWindow("Video Texture 1");

	// Create OpenCV camera capture
	// If multiple cameras are installed, please choose correct index
	g_Capture1 = cvCreateCameraCapture(0);
	g_Capture2 = cvCreateCameraCapture(1);
	assert(g_Capture1);
	assert(g_Capture2);
	// Initialize OpenGL
	InitAxesGL();
	glutMainLoop(); 


	return 0;

}




int OpenGLDraw::DrawAxes( int argc, char** argv, vector<vector<double>> M1, vector<vector<double>> M2)
{
	LSRMath *lsr1;
	lsr1=new LSRMath(); 
	vector<vector<vector<vector<double>>>> XYZcoords;
	if(lsr1->findAxesPoints( XYZcoords, 10.)!=0){return -1;}
	if(lsr1->findImageAxesPoints( XYZcoords, M1, ImCoords1)!=0){return -1;}
	if(lsr1->findImageAxesPoints( XYZcoords, M2, ImCoords2)!=0){return -1;}

	// Create GLUT Window
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(VIEWPORT_WIDTH*2, VIEWPORT_HEIGHT);

	g_hWindow1 = glutCreateWindow("Video Texture 1");

	// Create OpenCV camera capture
	// If multiple cameras are installed, please choose correct index
	g_Capture1 = cvCreateCameraCapture(0);
	g_Capture2 = cvCreateCameraCapture(1);
	assert(g_Capture1);
	assert(g_Capture2);
	// Initialize OpenGL
	InitAxesGL();
	glutMainLoop();

	return 0;
}

int OpenGLDraw::DrawLaserAxes(
							  int argc,
							  char** argv,
							  vector<vector<double>> MExt1,
							  vector<vector<double>> MExt2,
							  vector<vector<double>> MInt1,
							  vector<vector<double>> MInt2,
							  vector<vector<double>> vLaserX,
							  vector<vector<double>> vLaserY
							  )
{	
	LSRMath *lsr1;
	lsr1=new LSRMath(); 

	vector<double> RotXYZ1;
	vector<double> RotXYZ2;
	if(lsr1->FindRotationAngles( MExt1, RotXYZ1 )!=0){return -1;}
	if(lsr1->FindRotationAngles( MExt2, RotXYZ2 )!=0){return -1;}
	//vector<vector<double>> MRobotAtOrigin;
	//vector<vector<vector<vector<double>>>> XYZcoords;
//	if(lsr1->findAxesPoints( XYZcoords, 10.)!=0){return -1;}
//	if(lsr1->findImageAxesPoints( XYZcoords, M1, ImCoords1)!=0){return -1;}
//	if(lsr1->findImageAxesPoints( XYZcoords, M2, ImCoords2)!=0){return -1;}

	// Create GLUT Window
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(VIEWPORT_WIDTH*2, VIEWPORT_HEIGHT);

	g_hWindow1 = glutCreateWindow("Video Texture 1");

	// Create OpenCV camera capture
	// If multiple cameras are installed, please choose correct index
	g_Capture1 = cvCreateCameraCapture(0);
	g_Capture2 = cvCreateCameraCapture(1);
	assert(g_Capture1);
	assert(g_Capture2);
	// Initialize OpenGL

	double cparam[3][4];

	
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
			{ cparam[i][j]=MInt1[i][j]; }
		cparam[i][3]=0;
	}
	double intrinsic[16];
	argConvGLcpara2( cparam, 320, 240, 0.1, 5, intrinsic );
	glMatrixMode(GL_PROJECTION);
	glLoadMatrixd(intrinsic);


	InitLaserAxesGL();
	glutMainLoop();

	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
			{ cparam[i][j]=MInt2[i][j]; }
		cparam[i][3]=0;
	}

	argConvGLcpara2( cparam, 320, 240, 0.1, 5, intrinsic );
	glMatrixMode(GL_PROJECTION);
	glLoadMatrixd(intrinsic);


	return 0;
}


int OpenGLDraw::DrawBallTrajectory( 
	int argc, 
	char** argv, 
	vector<vector<double>> M1, 
	vector<vector<double>> M2,  
	vector<vector<double>> MExt1, 
	vector<vector<double>> MExt2,
	vector<vector<double>> MInt1,
	vector<vector<double>> MInt2,  
	vector<vector<double>> XYZarr,  
	vector<double> Tarr,
	vector<vector<double>> solveVar
	)
{	
	LSRMath *lsr1;
	lsr1=new LSRMath(); 

	vector<double> TarrSmooth(0);
	int PointCount = (int) Tarr.size();
	double GraphStep = 1.0;
	for (double i = 0, j = .1; i < (Tarr[PointCount-1] + 5.); i+=GraphStep)
	{

		TarrSmooth.push_back((double)i);
		if ((int)j < PointCount)
			if( Tarr[(int)j] < (i + GraphStep + 0.0) )
			{ 
				TarrSmooth.push_back(Tarr[(int)j]);
				j++;
			}

	}
	int PointCount2 = (int) TarrSmooth.size();
	


	ImCoords3d.resize(PointCount*2,
		vector<vector<double>>(3,
		vector<double>(1)
		));

	ImCoords1.resize(PointCount*2-1,vector<vector<vector<double>>>(2));
	ImCoords2.resize(PointCount*2-1,vector<vector<vector<double>>>(2));
	ImCoordsX1.resize(PointCount2-1,vector<vector<vector<double>>>(2));
	ImCoordsX2.resize(PointCount2-1,vector<vector<vector<double>>>(2));

	vector<vector<vector<double>>> targetPoint(PointCount);
	vector<vector<vector<double>>> targetPointSmooth(PointCount2);

	vector<vector<vector<double>>> coords (PointCount,vector<vector<double>>(3,vector<double>(1)));
	//vector<vector<vector<double>>> coords (PointCount,vector<vector<double>>(3,vector<double>(1)));
	for (int i = 0; i<PointCount; i++)
	{
		coords[i][0][0]=XYZarr[i][0];
		coords[i][1][0]=XYZarr[i][1];
		coords[i][2][0]=XYZarr[i][2];


		if (!lsr1->findParabolicPoint(solveVar,Tarr[i],targetPoint[i])==0) { return -1; }

		for (int j = 0; j<3; j++)
		{
			ImCoords3d[2*i][j][0]=coords[i][j][0];
			ImCoords3d[2*i+1][j][0]=targetPoint[i][j][0];
		}

		if (!lsr1->findImgCoordsFromWorld(targetPoint[i],M1,ImCoords1[2*i][0])==0) { return -1; }
		if (!lsr1->findImgCoordsFromWorld(targetPoint[i],M2,ImCoords2[2*i][0])==0) { return -1; }

		if (!lsr1->findImgCoordsFromWorld(coords[i],M1,ImCoords1[2*i][1])==0) { return -1; }
		if (!lsr1->findImgCoordsFromWorld(coords[i],M2,ImCoords2[2*i][1])==0) { return -1; }
	}

	for (int i = 0; i<PointCount-1; i++)
	{
		if (!lsr1->findImgCoordsFromWorld(coords[i],M1,ImCoords1[2*i+1][0])==0) { return -1; }
		if (!lsr1->findImgCoordsFromWorld(coords[i],M2,ImCoords2[2*i+1][0])==0) { return -1; }
		if (!lsr1->findImgCoordsFromWorld(coords[i+1],M1,ImCoords1[2*i+1][1])==0) { return -1; }
		if (!lsr1->findImgCoordsFromWorld(coords[i+1],M2,ImCoords2[2*i+1][1])==0) { return -1; }
	}

	
	
	for (int i = 0; i<PointCount2; i++)
	{ 
		if (!lsr1->findParabolicPoint(solveVar,TarrSmooth[i],targetPointSmooth[i])==0) { return -1; }
	}

	for (int i = 0; i<PointCount2-1; i++)
	{
		if (!lsr1->findImgCoordsFromWorld(targetPointSmooth[i],M1,ImCoordsX1[i][0])==0) { return -1; }
		if (!lsr1->findImgCoordsFromWorld(targetPointSmooth[i],M2,ImCoordsX2[i][0])==0) { return -1; }
		if (!lsr1->findImgCoordsFromWorld(targetPointSmooth[i+1],M1,ImCoordsX1[i][1])==0) { return -1; }
		if (!lsr1->findImgCoordsFromWorld(targetPointSmooth[i+1],M2,ImCoordsX2[i][1])==0) { return -1; }
	}





	// Create GLUT Window
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(VIEWPORT_WIDTH*2, VIEWPORT_HEIGHT);

	g_hWindow1 = glutCreateWindow("Video Texture 1");

	// Create OpenCV camera capture
	// If multiple cameras are installed, please choose correct index
	g_Capture1 = cvCreateCameraCapture(0);
	g_Capture2 = cvCreateCameraCapture(1);
	assert(g_Capture1);
	assert(g_Capture2);
	// Initialize OpenGL
	InitBallTrajectoryGL();
	glutMainLoop();






/*
	// Create GLUT Window
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(VIEWPORT_WIDTH, VIEWPORT_HEIGHT);

	g_hWindow1 = glutCreateWindow("Video Texture 1");

	// Create OpenCV camera capture
	// If multiple cameras are installed, please choose correct index
	g_Capture1 = cvCreateCameraCapture(0);
	g_Capture2 = cvCreateCameraCapture(1);
	assert(g_Capture1);
	assert(g_Capture2);
	// Initialize OpenGL

	vector<double> RotXYZ1;
	vector<double> RotXYZ2;
	if(lsr1->FindRotationAngles( MExt1, RotXYZ1 )!=0){return -1;}
	if(lsr1->FindRotationAngles( MExt2, RotXYZ2 )!=0){return -1;}

	double cparam[3][4];

	
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
			{ cparam[i][j]=MInt1[i][j]; }
		cparam[i][3]=0;
	}
	double intrinsic[16];
	argConvGLcpara2( cparam, 320, 240, 0.1, 5, intrinsic );
	glMatrixMode(GL_PROJECTION);
	glLoadMatrixd(intrinsic);


	InitBallTrajectoryGL();
	glutMainLoop();

	g_hWindow1 = glutCreateWindow("Video Texture 1");

	// Create OpenCV camera capture
	// If multiple cameras are installed, please choose correct index
	g_Capture1 = cvCreateCameraCapture(0);
	g_Capture2 = cvCreateCameraCapture(1);
	assert(g_Capture1);
	assert(g_Capture2);

	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
			{ cparam[i][j]=MInt2[i][j]; }
		cparam[i][3]=0;
	}

	argConvGLcpara2( cparam, 320, 240, 0.1, 5, intrinsic );
	glMatrixMode(GL_PROJECTION);
	glLoadMatrixd(intrinsic);

	InitBallTrajectoryGL();
	glutMainLoop();
*/
	return 0;
}

static void argConvGLcpara2( double cparam[3][4], int width, int height, double gnear, double gfar, double m[16] )
{
    double   icpara[3][4];
    double   trans[3][4];
    double   p[3][3], q[4][4];
    int      i, j;

    /* Usually we do not need to take care of this */
    /* If the specified cparam includes translation and rotation
      components, this function divides it into perspective projection
      component and translation/rotation components. */
    /* Usually cparam does not include translation/rotation components. */
    //if( arParamDecompMat(cparam, icpara, trans) < 0 ) {
    //    printf("gConvGLcpara: Parameter error!!\n");
    //    exit(0);
    //}

    /* Camera parameters are converted openGL representation. */
    /* Camera parameter represents the transformation from camera coordinates
       to screen coordinates[pixel]. OpenGL projection matrix represents the
       transformation from the camera coordinates to normalized view volume. */
    for( i = 0; i < 3; i++ ) {
        for( j = 0; j < 3; j++ ) {
            p[i][j] = icpara[i][j] / icpara[2][2];
        }
    }
    q[0][0] = (2.0 * p[0][0] / width);
    q[0][1] = (2.0 * p[0][1] / width);
    q[0][2] = ((2.0 * p[0][2] / width)  - 1.0);
    q[0][3] = 0.0;

    q[1][0] = 0.0;
    q[1][1] = (2.0 * p[1][1] / height);
    q[1][2] = ((2.0 * p[1][2] / height) - 1.0);
    q[1][3] = 0.0;

    q[2][0] = 0.0;
    q[2][1] = 0.0;
    q[2][2] = (gfar + gnear)/(gfar - gnear);
    q[2][3] = -2.0 * gfar * gnear / (gfar - gnear);

    q[3][0] = 0.0;
    q[3][1] = 0.0;
    q[3][2] = 1.0;
    q[3][3] = 0.0;


    /* This multiplies translation/rotation component to above calculated
       matrix. Usually we do not need to take care of this. */
    for( i = 0; i < 4; i++ ) {
        for( j = 0; j < 3; j++ ) {
            m[i+j*4] = q[i][0] * trans[0][j]
                     + q[i][1] * trans[1][j]
                     + q[i][2] * trans[2][j];
        }
        m[i+3*4] = q[i][0] * trans[0][3]
                 + q[i][1] * trans[1][3]
                 + q[i][2] * trans[2][3]
                 + q[i][3];
    }
}



OpenGLDraw::OpenGLDraw() 
{ 
}
OpenGLDraw::~OpenGLDraw() { }
